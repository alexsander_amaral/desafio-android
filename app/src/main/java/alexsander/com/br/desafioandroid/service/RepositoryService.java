package alexsander.com.br.desafioandroid.service;


import android.util.Log;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

import java.util.List;

import alexsander.com.br.desafioandroid.domain.Repository;

/**
 * Created by alexsander on 28/12/15.
 */
public class RepositoryService {
    private static final String BASE_URL = "https://api.github.com/search/repositories";

    public static List<Repository.Item> getRepositoryList(String q, String sort, String page) {
        HttpRequest request = HttpRequest
                .get(BASE_URL, true, 'q', q, "sort", sort, "page", page)
                .contentType("application/json");
        return parseJSON(request.body());
    }

    private static List<Repository.Item> parseJSON(String request) {

        List<Repository.Item> items;

        Gson gson = new Gson();
        Repository repository = gson.fromJson(request, Repository.class);
        items = repository.getItems();



        return items;
    }
}
