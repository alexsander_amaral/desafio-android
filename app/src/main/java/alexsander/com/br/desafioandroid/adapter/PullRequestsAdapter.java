package alexsander.com.br.desafioandroid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import alexsander.com.br.desafioandroid.R;
import alexsander.com.br.desafioandroid.domain.PullRequests;

/**
 * Created by alexsander on 02/01/16.
 */
public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsAdapter.PullRequestViewHolder>{

    private Context context;
    private List<PullRequests.PullRequest> pullRequests;
    private PullRequestOnClickListener pullRequestOnClickListener;

    public PullRequestsAdapter(Context context, List<PullRequests.PullRequest> pullRequests, PullRequestOnClickListener pullRequestOnClickListener) {
        this.context = context;
        this.pullRequests = pullRequests;
        this.pullRequestOnClickListener = pullRequestOnClickListener;
    }

    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_pull_request_list, parent, false);
        PullRequestViewHolder holder = new PullRequestViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final PullRequestViewHolder holder, final int position) {
        PullRequests.PullRequest pr = pullRequests.get(position);

        holder.tvTitle.setText(pr.getTitle());
        holder.tvBody.setText(pr.getBody());
        Picasso.with(context).load(pr.getUser().getAvatarUrl()).into(holder.ivOwner);
        holder.tvUsername.setText(pr.getUser().getLogin());
        holder.tvCreationDate.setText(pr.getCreatedAt().substring(0, 10));

        if (pullRequestOnClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pullRequestOnClickListener.onClickPullRequest(holder.itemView, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.pullRequests != null ? this.pullRequests.size() : 0;
    }

    public interface PullRequestOnClickListener {
        void onClickPullRequest(View view, int idx);
    }

    public class PullRequestViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvBody;
        ImageView ivOwner;
        TextView tvUsername;
        TextView tvCreationDate;

        public PullRequestViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_pull_request);
            tvBody = (TextView) itemView.findViewById(R.id.tv_body_pull_request);
            ivOwner = (ImageView) itemView.findViewById(R.id.iv_owner);
            tvUsername = (TextView) itemView.findViewById(R.id.tv_username);
            tvCreationDate = (TextView) itemView.findViewById(R.id.tv_creation_date);
        }
    }
}
