package alexsander.com.br.desafioandroid.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Repository implements Parcelable {

    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("incomplete_results")
    @Expose
    private Boolean incompleteResults;
    @SerializedName("items")
    @Expose
    private List<Item> items = new ArrayList<Item>();

    /**
     *
     * @return
     * The totalCount
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     *
     * @param totalCount
     * The total_count
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     *
     * @return
     * The incompleteResults
     */
    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    /**
     *
     * @param incompleteResults
     * The incomplete_results
     */
    public void setIncompleteResults(Boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    /**
     *
     * @return
     * The items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     *
     * @param items
     * The items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    public static class Item implements Parcelable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("full_name")
        @Expose
        private String fullName;
        @SerializedName("owner")
        @Expose
        private Owner owner;
        @SerializedName("private")
        @Expose
        private Boolean _private;
        @SerializedName("html_url")
        @Expose
        private String htmlUrl;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("fork")
        @Expose
        private Boolean fork;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("pushed_at")
        @Expose
        private String pushedAt;
        @SerializedName("homepage")
        @Expose
        private String homepage;
        @SerializedName("size")
        @Expose
        private Integer size;
        @SerializedName("stargazers_count")
        @Expose
        private Integer stargazersCount;
        @SerializedName("watchers_count")
        @Expose
        private Integer watchersCount;
        @SerializedName("language")
        @Expose
        private String language;
        @SerializedName("forks_count")
        @Expose
        private Integer forksCount;
        @SerializedName("open_issues_count")
        @Expose
        private Integer openIssuesCount;
        @SerializedName("master_branch")
        @Expose
        private String masterBranch;
        @SerializedName("default_branch")
        @Expose
        private String defaultBranch;
        @SerializedName("score")
        @Expose
        private Double score;

        /**
         *
         * @return
         * The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         * The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         * The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         * The fullName
         */
        public String getFullName() {
            return fullName;
        }

        /**
         *
         * @param fullName
         * The full_name
         */
        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        /**
         *
         * @return
         * The owner
         */
        public Owner getOwner() {
            return owner;
        }

        /**
         *
         * @param owner
         * The owner
         */
        public void setOwner(Owner owner) {
            this.owner = owner;
        }

        /**
         *
         * @return
         * The _private
         */
        public Boolean getPrivate() {
            return _private;
        }

        /**
         *
         * @param _private
         * The private
         */
        public void setPrivate(Boolean _private) {
            this._private = _private;
        }

        /**
         *
         * @return
         * The htmlUrl
         */
        public String getHtmlUrl() {
            return htmlUrl;
        }

        /**
         *
         * @param htmlUrl
         * The html_url
         */
        public void setHtmlUrl(String htmlUrl) {
            this.htmlUrl = htmlUrl;
        }

        /**
         *
         * @return
         * The description
         */
        public String getDescription() {
            return description;
        }

        /**
         *
         * @param description
         * The description
         */
        public void setDescription(String description) {
            this.description = description;
        }

        /**
         *
         * @return
         * The fork
         */
        public Boolean getFork() {
            return fork;
        }

        /**
         *
         * @param fork
         * The fork
         */
        public void setFork(Boolean fork) {
            this.fork = fork;
        }

        /**
         *
         * @return
         * The url
         */
        public String getUrl() {
            return url;
        }

        /**
         *
         * @param url
         * The url
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         *
         * @return
         * The createdAt
         */
        public String getCreatedAt() {
            return createdAt;
        }

        /**
         *
         * @param createdAt
         * The created_at
         */
        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        /**
         *
         * @return
         * The updatedAt
         */
        public String getUpdatedAt() {
            return updatedAt;
        }

        /**
         *
         * @param updatedAt
         * The updated_at
         */
        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        /**
         *
         * @return
         * The pushedAt
         */
        public String getPushedAt() {
            return pushedAt;
        }

        /**
         *
         * @param pushedAt
         * The pushed_at
         */
        public void setPushedAt(String pushedAt) {
            this.pushedAt = pushedAt;
        }

        /**
         *
         * @return
         * The homepage
         */
        public String getHomepage() {
            return homepage;
        }

        /**
         *
         * @param homepage
         * The homepage
         */
        public void setHomepage(String homepage) {
            this.homepage = homepage;
        }

        /**
         *
         * @return
         * The size
         */
        public Integer getSize() {
            return size;
        }

        /**
         *
         * @param size
         * The size
         */
        public void setSize(Integer size) {
            this.size = size;
        }

        /**
         *
         * @return
         * The stargazersCount
         */
        public Integer getStargazersCount() {
            return stargazersCount;
        }

        /**
         *
         * @param stargazersCount
         * The stargazers_count
         */
        public void setStargazersCount(Integer stargazersCount) {
            this.stargazersCount = stargazersCount;
        }

        /**
         *
         * @return
         * The watchersCount
         */
        public Integer getWatchersCount() {
            return watchersCount;
        }

        /**
         *
         * @param watchersCount
         * The watchers_count
         */
        public void setWatchersCount(Integer watchersCount) {
            this.watchersCount = watchersCount;
        }

        /**
         *
         * @return
         * The language
         */
        public String getLanguage() {
            return language;
        }

        /**
         *
         * @param language
         * The language
         */
        public void setLanguage(String language) {
            this.language = language;
        }

        /**
         *
         * @return
         * The forksCount
         */
        public Integer getForksCount() {
            return forksCount;
        }

        /**
         *
         * @param forksCount
         * The forks_count
         */
        public void setForksCount(Integer forksCount) {
            this.forksCount = forksCount;
        }

        /**
         *
         * @return
         * The openIssuesCount
         */
        public Integer getOpenIssuesCount() {
            return openIssuesCount;
        }

        /**
         *
         * @param openIssuesCount
         * The open_issues_count
         */
        public void setOpenIssuesCount(Integer openIssuesCount) {
            this.openIssuesCount = openIssuesCount;
        }

        /**
         *
         * @return
         * The masterBranch
         */
        public String getMasterBranch() {
            return masterBranch;
        }

        /**
         *
         * @param masterBranch
         * The master_branch
         */
        public void setMasterBranch(String masterBranch) {
            this.masterBranch = masterBranch;
        }

        /**
         *
         * @return
         * The defaultBranch
         */
        public String getDefaultBranch() {
            return defaultBranch;
        }

        /**
         *
         * @param defaultBranch
         * The default_branch
         */
        public void setDefaultBranch(String defaultBranch) {
            this.defaultBranch = defaultBranch;
        }

        /**
         *
         * @return
         * The score
         */
        public Double getScore() {
            return score;
        }

        /**
         *
         * @param score
         * The score
         */
        public void setScore(Double score) {
            this.score = score;
        }

        public class Owner implements Parcelable {

            @SerializedName("login")
            @Expose
            private String login;
            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("avatar_url")
            @Expose
            private String avatarUrl;
            @SerializedName("gravatar_id")
            @Expose
            private String gravatarId;
            @SerializedName("url")
            @Expose
            private String url;
            @SerializedName("received_events_url")
            @Expose
            private String receivedEventsUrl;
            @SerializedName("type")
            @Expose
            private String type;

            /**
             *
             * @return
             * The login
             */
            public String getLogin() {
                return login;
            }

            /**
             *
             * @param login
             * The login
             */
            public void setLogin(String login) {
                this.login = login;
            }

            /**
             *
             * @return
             * The id
             */
            public Integer getId() {
                return id;
            }

            /**
             *
             * @param id
             * The id
             */
            public void setId(Integer id) {
                this.id = id;
            }

            /**
             *
             * @return
             * The avatarUrl
             */
            public String getAvatarUrl() {
                return avatarUrl;
            }

            /**
             *
             * @param avatarUrl
             * The avatar_url
             */
            public void setAvatarUrl(String avatarUrl) {
                this.avatarUrl = avatarUrl;
            }

            /**
             *
             * @return
             * The gravatarId
             */
            public String getGravatarId() {
                return gravatarId;
            }

            /**
             *
             * @param gravatarId
             * The gravatar_id
             */
            public void setGravatarId(String gravatarId) {
                this.gravatarId = gravatarId;
            }

            /**
             *
             * @return
             * The url
             */
            public String getUrl() {
                return url;
            }

            /**
             *
             * @param url
             * The url
             */
            public void setUrl(String url) {
                this.url = url;
            }

            /**
             *
             * @return
             * The receivedEventsUrl
             */
            public String getReceivedEventsUrl() {
                return receivedEventsUrl;
            }

            /**
             *
             * @param receivedEventsUrl
             * The received_events_url
             */
            public void setReceivedEventsUrl(String receivedEventsUrl) {
                this.receivedEventsUrl = receivedEventsUrl;
            }

            /**
             *
             * @return
             * The type
             */
            public String getType() {
                return type;
            }

            /**
             *
             * @param type
             * The type
             */
            public void setType(String type) {
                this.type = type;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.login);
                dest.writeValue(this.id);
                dest.writeString(this.avatarUrl);
                dest.writeString(this.gravatarId);
                dest.writeString(this.url);
                dest.writeString(this.receivedEventsUrl);
                dest.writeString(this.type);
            }

            public Owner() {
            }

            protected Owner(Parcel in) {
                this.login = in.readString();
                this.id = (Integer) in.readValue(Integer.class.getClassLoader());
                this.avatarUrl = in.readString();
                this.gravatarId = in.readString();
                this.url = in.readString();
                this.receivedEventsUrl = in.readString();
                this.type = in.readString();
            }

            public final Creator<Owner> CREATOR = new Creator<Owner>() {
                public Owner createFromParcel(Parcel source) {
                    return new Owner(source);
                }

                public Owner[] newArray(int size) {
                    return new Owner[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(this.id);
            dest.writeString(this.name);
            dest.writeString(this.fullName);
            dest.writeParcelable(this.owner, flags);
            dest.writeValue(this._private);
            dest.writeString(this.htmlUrl);
            dest.writeString(this.description);
            dest.writeValue(this.fork);
            dest.writeString(this.url);
            dest.writeString(this.createdAt);
            dest.writeString(this.updatedAt);
            dest.writeString(this.pushedAt);
            dest.writeString(this.homepage);
            dest.writeValue(this.size);
            dest.writeValue(this.stargazersCount);
            dest.writeValue(this.watchersCount);
            dest.writeString(this.language);
            dest.writeValue(this.forksCount);
            dest.writeValue(this.openIssuesCount);
            dest.writeString(this.masterBranch);
            dest.writeString(this.defaultBranch);
            dest.writeValue(this.score);
        }

        public Item() {
        }

        protected Item(Parcel in) {
            this.id = (Integer) in.readValue(Integer.class.getClassLoader());
            this.name = in.readString();
            this.fullName = in.readString();
            this.owner = in.readParcelable(Owner.class.getClassLoader());
            this._private = (Boolean) in.readValue(Boolean.class.getClassLoader());
            this.htmlUrl = in.readString();
            this.description = in.readString();
            this.fork = (Boolean) in.readValue(Boolean.class.getClassLoader());
            this.url = in.readString();
            this.createdAt = in.readString();
            this.updatedAt = in.readString();
            this.pushedAt = in.readString();
            this.homepage = in.readString();
            this.size = (Integer) in.readValue(Integer.class.getClassLoader());
            this.stargazersCount = (Integer) in.readValue(Integer.class.getClassLoader());
            this.watchersCount = (Integer) in.readValue(Integer.class.getClassLoader());
            this.language = in.readString();
            this.forksCount = (Integer) in.readValue(Integer.class.getClassLoader());
            this.openIssuesCount = (Integer) in.readValue(Integer.class.getClassLoader());
            this.masterBranch = in.readString();
            this.defaultBranch = in.readString();
            this.score = (Double) in.readValue(Double.class.getClassLoader());
        }

        public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
            public Item createFromParcel(Parcel source) {
                return new Item(source);
            }

            public Item[] newArray(int size) {
                return new Item[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.totalCount);
        dest.writeValue(this.incompleteResults);
        dest.writeTypedList(items);
    }

    public Repository() {
    }

    protected Repository(Parcel in) {
        this.totalCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.incompleteResults = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.items = in.createTypedArrayList(Item.CREATOR);
    }

    public static final Parcelable.Creator<Repository> CREATOR = new Parcelable.Creator<Repository>() {
        public Repository createFromParcel(Parcel source) {
            return new Repository(source);
        }

        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };
}
