package alexsander.com.br.desafioandroid.domain;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class PullRequests implements Parcelable {
    private List<PullRequest> pullRequests;

    public List<PullRequest> getPullRequests() {
        return pullRequests;
    }

    public void setPullRequests(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }

    public class PullRequest implements Parcelable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("html_url")
        @Expose
        private String htmlUrl;
        @SerializedName("diff_url")
        @Expose
        private String diffUrl;
        @SerializedName("patch_url")
        @Expose
        private String patchUrl;
        @SerializedName("issue_url")
        @Expose
        private String issueUrl;
        @SerializedName("commits_url")
        @Expose
        private String commitsUrl;
        @SerializedName("review_comments_url")
        @Expose
        private String reviewCommentsUrl;
        @SerializedName("review_comment_url")
        @Expose
        private String reviewCommentUrl;
        @SerializedName("comments_url")
        @Expose
        private String commentsUrl;
        @SerializedName("statuses_url")
        @Expose
        private String statusesUrl;
        @SerializedName("number")
        @Expose
        private Integer number;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("body")
        @Expose
        private String body;
        @SerializedName("assignee")
        @Expose
        private Assignee assignee;
        @SerializedName("milestone")
        @Expose
        private Milestone milestone;
        @SerializedName("locked")
        @Expose
        private Boolean locked;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("closed_at")
        @Expose
        private String closedAt;
        @SerializedName("merged_at")
        @Expose
        private String mergedAt;
        @SerializedName("head")
        @Expose
        private Head head;
        @SerializedName("base")
        @Expose
        private Base base;
        @SerializedName("_links")
        @Expose
        private Links Links;
        @SerializedName("user")
        @Expose
        private User__ user;

        /**
         *
         * @return
         * The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         * The url
         */
        public String getUrl() {
            return url;
        }

        /**
         *
         * @param url
         * The url
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         *
         * @return
         * The htmlUrl
         */
        public String getHtmlUrl() {
            return htmlUrl;
        }

        /**
         *
         * @param htmlUrl
         * The html_url
         */
        public void setHtmlUrl(String htmlUrl) {
            this.htmlUrl = htmlUrl;
        }

        /**
         *
         * @return
         * The diffUrl
         */
        public String getDiffUrl() {
            return diffUrl;
        }

        /**
         *
         * @param diffUrl
         * The diff_url
         */
        public void setDiffUrl(String diffUrl) {
            this.diffUrl = diffUrl;
        }

        /**
         *
         * @return
         * The patchUrl
         */
        public String getPatchUrl() {
            return patchUrl;
        }

        /**
         *
         * @param patchUrl
         * The patch_url
         */
        public void setPatchUrl(String patchUrl) {
            this.patchUrl = patchUrl;
        }

        /**
         *
         * @return
         * The issueUrl
         */
        public String getIssueUrl() {
            return issueUrl;
        }

        /**
         *
         * @param issueUrl
         * The issue_url
         */
        public void setIssueUrl(String issueUrl) {
            this.issueUrl = issueUrl;
        }

        /**
         *
         * @return
         * The commitsUrl
         */
        public String getCommitsUrl() {
            return commitsUrl;
        }

        /**
         *
         * @param commitsUrl
         * The commits_url
         */
        public void setCommitsUrl(String commitsUrl) {
            this.commitsUrl = commitsUrl;
        }

        /**
         *
         * @return
         * The reviewCommentsUrl
         */
        public String getReviewCommentsUrl() {
            return reviewCommentsUrl;
        }

        /**
         *
         * @param reviewCommentsUrl
         * The review_comments_url
         */
        public void setReviewCommentsUrl(String reviewCommentsUrl) {
            this.reviewCommentsUrl = reviewCommentsUrl;
        }

        /**
         *
         * @return
         * The reviewCommentUrl
         */
        public String getReviewCommentUrl() {
            return reviewCommentUrl;
        }

        /**
         *
         * @param reviewCommentUrl
         * The review_comment_url
         */
        public void setReviewCommentUrl(String reviewCommentUrl) {
            this.reviewCommentUrl = reviewCommentUrl;
        }

        /**
         *
         * @return
         * The commentsUrl
         */
        public String getCommentsUrl() {
            return commentsUrl;
        }

        /**
         *
         * @param commentsUrl
         * The comments_url
         */
        public void setCommentsUrl(String commentsUrl) {
            this.commentsUrl = commentsUrl;
        }

        /**
         *
         * @return
         * The statusesUrl
         */
        public String getStatusesUrl() {
            return statusesUrl;
        }

        /**
         *
         * @param statusesUrl
         * The statuses_url
         */
        public void setStatusesUrl(String statusesUrl) {
            this.statusesUrl = statusesUrl;
        }

        /**
         *
         * @return
         * The number
         */
        public Integer getNumber() {
            return number;
        }

        /**
         *
         * @param number
         * The number
         */
        public void setNumber(Integer number) {
            this.number = number;
        }

        /**
         *
         * @return
         * The state
         */
        public String getState() {
            return state;
        }

        /**
         *
         * @param state
         * The state
         */
        public void setState(String state) {
            this.state = state;
        }

        /**
         *
         * @return
         * The title
         */
        public String getTitle() {
            return title;
        }

        /**
         *
         * @param title
         * The title
         */
        public void setTitle(String title) {
            this.title = title;
        }

        /**
         *
         * @return
         * The body
         */
        public String getBody() {
            return body;
        }

        /**
         *
         * @param body
         * The body
         */
        public void setBody(String body) {
            this.body = body;
        }

        /**
         *
         * @return
         * The assignee
         */
        public Assignee getAssignee() {
            return assignee;
        }

        /**
         *
         * @param assignee
         * The assignee
         */
        public void setAssignee(Assignee assignee) {
            this.assignee = assignee;
        }

        /**
         *
         * @return
         * The milestone
         */
        public Milestone getMilestone() {
            return milestone;
        }

        /**
         *
         * @param milestone
         * The milestone
         */
        public void setMilestone(Milestone milestone) {
            this.milestone = milestone;
        }

        /**
         *
         * @return
         * The locked
         */
        public Boolean getLocked() {
            return locked;
        }

        /**
         *
         * @param locked
         * The locked
         */
        public void setLocked(Boolean locked) {
            this.locked = locked;
        }

        /**
         *
         * @return
         * The createdAt
         */
        public String getCreatedAt() {
            return createdAt;
        }

        /**
         *
         * @param createdAt
         * The created_at
         */
        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        /**
         *
         * @return
         * The updatedAt
         */
        public String getUpdatedAt() {
            return updatedAt;
        }

        /**
         *
         * @param updatedAt
         * The updated_at
         */
        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        /**
         *
         * @return
         * The closedAt
         */
        public String getClosedAt() {
            return closedAt;
        }

        /**
         *
         * @param closedAt
         * The closed_at
         */
        public void setClosedAt(String closedAt) {
            this.closedAt = closedAt;
        }

        /**
         *
         * @return
         * The mergedAt
         */
        public String getMergedAt() {
            return mergedAt;
        }

        /**
         *
         * @param mergedAt
         * The merged_at
         */
        public void setMergedAt(String mergedAt) {
            this.mergedAt = mergedAt;
        }

        /**
         *
         * @return
         * The head
         */
        public Head getHead() {
            return head;
        }

        /**
         *
         * @param head
         * The head
         */
        public void setHead(Head head) {
            this.head = head;
        }

        /**
         *
         * @return
         * The base
         */
        public Base getBase() {
            return base;
        }

        /**
         *
         * @param base
         * The base
         */
        public void setBase(Base base) {
            this.base = base;
        }

        /**
         *
         * @return
         * The Links
         */
        public Links getLinks() {
            return Links;
        }

        /**
         *
         * @param Links
         * The _links
         */
        public void setLinks(Links Links) {
            this.Links = Links;
        }

        /**
         *
         * @return
         * The user
         */
        public User__ getUser() {
            return user;
        }

        /**
         *
         * @param user
         * The user
         */
        public void setUser(User__ user) {
            this.user = user;
        }

        public class User__ implements Parcelable {

            @SerializedName("login")
            @Expose
            private String login;
            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("avatar_url")
            @Expose
            private String avatarUrl;
            @SerializedName("gravatar_id")
            @Expose
            private String gravatarId;
            @SerializedName("url")
            @Expose
            private String url;
            @SerializedName("html_url")
            @Expose
            private String htmlUrl;
            @SerializedName("followers_url")
            @Expose
            private String followersUrl;
            @SerializedName("following_url")
            @Expose
            private String followingUrl;
            @SerializedName("gists_url")
            @Expose
            private String gistsUrl;
            @SerializedName("starred_url")
            @Expose
            private String starredUrl;
            @SerializedName("subscriptions_url")
            @Expose
            private String subscriptionsUrl;
            @SerializedName("organizations_url")
            @Expose
            private String organizationsUrl;
            @SerializedName("repos_url")
            @Expose
            private String reposUrl;
            @SerializedName("events_url")
            @Expose
            private String eventsUrl;
            @SerializedName("received_events_url")
            @Expose
            private String receivedEventsUrl;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("site_admin")
            @Expose
            private Boolean siteAdmin;

            /**
             *
             * @return
             * The login
             */
            public String getLogin() {
                return login;
            }

            /**
             *
             * @param login
             * The login
             */
            public void setLogin(String login) {
                this.login = login;
            }

            /**
             *
             * @return
             * The id
             */
            public Integer getId() {
                return id;
            }

            /**
             *
             * @param id
             * The id
             */
            public void setId(Integer id) {
                this.id = id;
            }

            /**
             *
             * @return
             * The avatarUrl
             */
            public String getAvatarUrl() {
                return avatarUrl;
            }

            /**
             *
             * @param avatarUrl
             * The avatar_url
             */
            public void setAvatarUrl(String avatarUrl) {
                this.avatarUrl = avatarUrl;
            }

            /**
             *
             * @return
             * The gravatarId
             */
            public String getGravatarId() {
                return gravatarId;
            }

            /**
             *
             * @param gravatarId
             * The gravatar_id
             */
            public void setGravatarId(String gravatarId) {
                this.gravatarId = gravatarId;
            }

            /**
             *
             * @return
             * The url
             */
            public String getUrl() {
                return url;
            }

            /**
             *
             * @param url
             * The url
             */
            public void setUrl(String url) {
                this.url = url;
            }

            /**
             *
             * @return
             * The htmlUrl
             */
            public String getHtmlUrl() {
                return htmlUrl;
            }

            /**
             *
             * @param htmlUrl
             * The html_url
             */
            public void setHtmlUrl(String htmlUrl) {
                this.htmlUrl = htmlUrl;
            }

            /**
             *
             * @return
             * The followersUrl
             */
            public String getFollowersUrl() {
                return followersUrl;
            }

            /**
             *
             * @param followersUrl
             * The followers_url
             */
            public void setFollowersUrl(String followersUrl) {
                this.followersUrl = followersUrl;
            }

            /**
             *
             * @return
             * The followingUrl
             */
            public String getFollowingUrl() {
                return followingUrl;
            }

            /**
             *
             * @param followingUrl
             * The following_url
             */
            public void setFollowingUrl(String followingUrl) {
                this.followingUrl = followingUrl;
            }

            /**
             *
             * @return
             * The gistsUrl
             */
            public String getGistsUrl() {
                return gistsUrl;
            }

            /**
             *
             * @param gistsUrl
             * The gists_url
             */
            public void setGistsUrl(String gistsUrl) {
                this.gistsUrl = gistsUrl;
            }

            /**
             *
             * @return
             * The starredUrl
             */
            public String getStarredUrl() {
                return starredUrl;
            }

            /**
             *
             * @param starredUrl
             * The starred_url
             */
            public void setStarredUrl(String starredUrl) {
                this.starredUrl = starredUrl;
            }

            /**
             *
             * @return
             * The subscriptionsUrl
             */
            public String getSubscriptionsUrl() {
                return subscriptionsUrl;
            }

            /**
             *
             * @param subscriptionsUrl
             * The subscriptions_url
             */
            public void setSubscriptionsUrl(String subscriptionsUrl) {
                this.subscriptionsUrl = subscriptionsUrl;
            }

            /**
             *
             * @return
             * The organizationsUrl
             */
            public String getOrganizationsUrl() {
                return organizationsUrl;
            }

            /**
             *
             * @param organizationsUrl
             * The organizations_url
             */
            public void setOrganizationsUrl(String organizationsUrl) {
                this.organizationsUrl = organizationsUrl;
            }

            /**
             *
             * @return
             * The reposUrl
             */
            public String getReposUrl() {
                return reposUrl;
            }

            /**
             *
             * @param reposUrl
             * The repos_url
             */
            public void setReposUrl(String reposUrl) {
                this.reposUrl = reposUrl;
            }

            /**
             *
             * @return
             * The eventsUrl
             */
            public String getEventsUrl() {
                return eventsUrl;
            }

            /**
             *
             * @param eventsUrl
             * The events_url
             */
            public void setEventsUrl(String eventsUrl) {
                this.eventsUrl = eventsUrl;
            }

            /**
             *
             * @return
             * The receivedEventsUrl
             */
            public String getReceivedEventsUrl() {
                return receivedEventsUrl;
            }

            /**
             *
             * @param receivedEventsUrl
             * The received_events_url
             */
            public void setReceivedEventsUrl(String receivedEventsUrl) {
                this.receivedEventsUrl = receivedEventsUrl;
            }

            /**
             *
             * @return
             * The type
             */
            public String getType() {
                return type;
            }

            /**
             *
             * @param type
             * The type
             */
            public void setType(String type) {
                this.type = type;
            }

            /**
             *
             * @return
             * The siteAdmin
             */
            public Boolean getSiteAdmin() {
                return siteAdmin;
            }

            /**
             *
             * @param siteAdmin
             * The site_admin
             */
            public void setSiteAdmin(Boolean siteAdmin) {
                this.siteAdmin = siteAdmin;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.login);
                dest.writeValue(this.id);
                dest.writeString(this.avatarUrl);
                dest.writeString(this.gravatarId);
                dest.writeString(this.url);
                dest.writeString(this.htmlUrl);
                dest.writeString(this.followersUrl);
                dest.writeString(this.followingUrl);
                dest.writeString(this.gistsUrl);
                dest.writeString(this.starredUrl);
                dest.writeString(this.subscriptionsUrl);
                dest.writeString(this.organizationsUrl);
                dest.writeString(this.reposUrl);
                dest.writeString(this.eventsUrl);
                dest.writeString(this.receivedEventsUrl);
                dest.writeString(this.type);
                dest.writeValue(this.siteAdmin);
            }

            public User__() {
            }

            protected User__(Parcel in) {
                this.login = in.readString();
                this.id = (Integer) in.readValue(Integer.class.getClassLoader());
                this.avatarUrl = in.readString();
                this.gravatarId = in.readString();
                this.url = in.readString();
                this.htmlUrl = in.readString();
                this.followersUrl = in.readString();
                this.followingUrl = in.readString();
                this.gistsUrl = in.readString();
                this.starredUrl = in.readString();
                this.subscriptionsUrl = in.readString();
                this.organizationsUrl = in.readString();
                this.reposUrl = in.readString();
                this.eventsUrl = in.readString();
                this.receivedEventsUrl = in.readString();
                this.type = in.readString();
                this.siteAdmin = (Boolean) in.readValue(Boolean.class.getClassLoader());
            }

            public final Creator<User__> CREATOR = new Creator<User__>() {
                public User__ createFromParcel(Parcel source) {
                    return new User__(source);
                }

                public User__[] newArray(int size) {
                    return new User__[size];
                }
            };
        }

        public class Head implements Parcelable {

            @SerializedName("label")
            @Expose
            private String label;
            @SerializedName("ref")
            @Expose
            private String ref;
            @SerializedName("sha")
            @Expose
            private String sha;
            @SerializedName("user")
            @Expose
            private User user;
            @SerializedName("repo")
            @Expose
            private Repo repo;

            /**
             *
             * @return
             * The label
             */
            public String getLabel() {
                return label;
            }

            /**
             *
             * @param label
             * The label
             */
            public void setLabel(String label) {
                this.label = label;
            }

            /**
             *
             * @return
             * The ref
             */
            public String getRef() {
                return ref;
            }

            /**
             *
             * @param ref
             * The ref
             */
            public void setRef(String ref) {
                this.ref = ref;
            }

            /**
             *
             * @return
             * The sha
             */
            public String getSha() {
                return sha;
            }

            /**
             *
             * @param sha
             * The sha
             */
            public void setSha(String sha) {
                this.sha = sha;
            }

            /**
             *
             * @return
             * The user
             */
            public User getUser() {
                return user;
            }

            /**
             *
             * @param user
             * The user
             */
            public void setUser(User user) {
                this.user = user;
            }

            /**
             *
             * @return
             * The repo
             */
            public Repo getRepo() {
                return repo;
            }

            /**
             *
             * @param repo
             * The repo
             */
            public void setRepo(Repo repo) {
                this.repo = repo;
            }

            public class User implements Parcelable {

                @SerializedName("login")
                @Expose
                private String login;
                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("avatar_url")
                @Expose
                private String avatarUrl;
                @SerializedName("gravatar_id")
                @Expose
                private String gravatarId;
                @SerializedName("url")
                @Expose
                private String url;
                @SerializedName("html_url")
                @Expose
                private String htmlUrl;
                @SerializedName("followers_url")
                @Expose
                private String followersUrl;
                @SerializedName("following_url")
                @Expose
                private String followingUrl;
                @SerializedName("gists_url")
                @Expose
                private String gistsUrl;
                @SerializedName("starred_url")
                @Expose
                private String starredUrl;
                @SerializedName("subscriptions_url")
                @Expose
                private String subscriptionsUrl;
                @SerializedName("organizations_url")
                @Expose
                private String organizationsUrl;
                @SerializedName("repos_url")
                @Expose
                private String reposUrl;
                @SerializedName("events_url")
                @Expose
                private String eventsUrl;
                @SerializedName("received_events_url")
                @Expose
                private String receivedEventsUrl;
                @SerializedName("type")
                @Expose
                private String type;
                @SerializedName("site_admin")
                @Expose
                private Boolean siteAdmin;

                /**
                 *
                 * @return
                 * The login
                 */
                public String getLogin() {
                    return login;
                }

                /**
                 *
                 * @param login
                 * The login
                 */
                public void setLogin(String login) {
                    this.login = login;
                }

                /**
                 *
                 * @return
                 * The id
                 */
                public Integer getId() {
                    return id;
                }

                /**
                 *
                 * @param id
                 * The id
                 */
                public void setId(Integer id) {
                    this.id = id;
                }

                /**
                 *
                 * @return
                 * The avatarUrl
                 */
                public String getAvatarUrl() {
                    return avatarUrl;
                }

                /**
                 *
                 * @param avatarUrl
                 * The avatar_url
                 */
                public void setAvatarUrl(String avatarUrl) {
                    this.avatarUrl = avatarUrl;
                }

                /**
                 *
                 * @return
                 * The gravatarId
                 */
                public String getGravatarId() {
                    return gravatarId;
                }

                /**
                 *
                 * @param gravatarId
                 * The gravatar_id
                 */
                public void setGravatarId(String gravatarId) {
                    this.gravatarId = gravatarId;
                }

                /**
                 *
                 * @return
                 * The url
                 */
                public String getUrl() {
                    return url;
                }

                /**
                 *
                 * @param url
                 * The url
                 */
                public void setUrl(String url) {
                    this.url = url;
                }

                /**
                 *
                 * @return
                 * The htmlUrl
                 */
                public String getHtmlUrl() {
                    return htmlUrl;
                }

                /**
                 *
                 * @param htmlUrl
                 * The html_url
                 */
                public void setHtmlUrl(String htmlUrl) {
                    this.htmlUrl = htmlUrl;
                }

                /**
                 *
                 * @return
                 * The followersUrl
                 */
                public String getFollowersUrl() {
                    return followersUrl;
                }

                /**
                 *
                 * @param followersUrl
                 * The followers_url
                 */
                public void setFollowersUrl(String followersUrl) {
                    this.followersUrl = followersUrl;
                }

                /**
                 *
                 * @return
                 * The followingUrl
                 */
                public String getFollowingUrl() {
                    return followingUrl;
                }

                /**
                 *
                 * @param followingUrl
                 * The following_url
                 */
                public void setFollowingUrl(String followingUrl) {
                    this.followingUrl = followingUrl;
                }

                /**
                 *
                 * @return
                 * The gistsUrl
                 */
                public String getGistsUrl() {
                    return gistsUrl;
                }

                /**
                 *
                 * @param gistsUrl
                 * The gists_url
                 */
                public void setGistsUrl(String gistsUrl) {
                    this.gistsUrl = gistsUrl;
                }

                /**
                 *
                 * @return
                 * The starredUrl
                 */
                public String getStarredUrl() {
                    return starredUrl;
                }

                /**
                 *
                 * @param starredUrl
                 * The starred_url
                 */
                public void setStarredUrl(String starredUrl) {
                    this.starredUrl = starredUrl;
                }

                /**
                 *
                 * @return
                 * The subscriptionsUrl
                 */
                public String getSubscriptionsUrl() {
                    return subscriptionsUrl;
                }

                /**
                 *
                 * @param subscriptionsUrl
                 * The subscriptions_url
                 */
                public void setSubscriptionsUrl(String subscriptionsUrl) {
                    this.subscriptionsUrl = subscriptionsUrl;
                }

                /**
                 *
                 * @return
                 * The organizationsUrl
                 */
                public String getOrganizationsUrl() {
                    return organizationsUrl;
                }

                /**
                 *
                 * @param organizationsUrl
                 * The organizations_url
                 */
                public void setOrganizationsUrl(String organizationsUrl) {
                    this.organizationsUrl = organizationsUrl;
                }

                /**
                 *
                 * @return
                 * The reposUrl
                 */
                public String getReposUrl() {
                    return reposUrl;
                }

                /**
                 *
                 * @param reposUrl
                 * The repos_url
                 */
                public void setReposUrl(String reposUrl) {
                    this.reposUrl = reposUrl;
                }

                /**
                 *
                 * @return
                 * The eventsUrl
                 */
                public String getEventsUrl() {
                    return eventsUrl;
                }

                /**
                 *
                 * @param eventsUrl
                 * The events_url
                 */
                public void setEventsUrl(String eventsUrl) {
                    this.eventsUrl = eventsUrl;
                }

                /**
                 *
                 * @return
                 * The receivedEventsUrl
                 */
                public String getReceivedEventsUrl() {
                    return receivedEventsUrl;
                }

                /**
                 *
                 * @param receivedEventsUrl
                 * The received_events_url
                 */
                public void setReceivedEventsUrl(String receivedEventsUrl) {
                    this.receivedEventsUrl = receivedEventsUrl;
                }

                /**
                 *
                 * @return
                 * The type
                 */
                public String getType() {
                    return type;
                }

                /**
                 *
                 * @param type
                 * The type
                 */
                public void setType(String type) {
                    this.type = type;
                }

                /**
                 *
                 * @return
                 * The siteAdmin
                 */
                public Boolean getSiteAdmin() {
                    return siteAdmin;
                }

                /**
                 *
                 * @param siteAdmin
                 * The site_admin
                 */
                public void setSiteAdmin(Boolean siteAdmin) {
                    this.siteAdmin = siteAdmin;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.login);
                    dest.writeValue(this.id);
                    dest.writeString(this.avatarUrl);
                    dest.writeString(this.gravatarId);
                    dest.writeString(this.url);
                    dest.writeString(this.htmlUrl);
                    dest.writeString(this.followersUrl);
                    dest.writeString(this.followingUrl);
                    dest.writeString(this.gistsUrl);
                    dest.writeString(this.starredUrl);
                    dest.writeString(this.subscriptionsUrl);
                    dest.writeString(this.organizationsUrl);
                    dest.writeString(this.reposUrl);
                    dest.writeString(this.eventsUrl);
                    dest.writeString(this.receivedEventsUrl);
                    dest.writeString(this.type);
                    dest.writeValue(this.siteAdmin);
                }

                public User() {
                }

                protected User(Parcel in) {
                    this.login = in.readString();
                    this.id = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.avatarUrl = in.readString();
                    this.gravatarId = in.readString();
                    this.url = in.readString();
                    this.htmlUrl = in.readString();
                    this.followersUrl = in.readString();
                    this.followingUrl = in.readString();
                    this.gistsUrl = in.readString();
                    this.starredUrl = in.readString();
                    this.subscriptionsUrl = in.readString();
                    this.organizationsUrl = in.readString();
                    this.reposUrl = in.readString();
                    this.eventsUrl = in.readString();
                    this.receivedEventsUrl = in.readString();
                    this.type = in.readString();
                    this.siteAdmin = (Boolean) in.readValue(Boolean.class.getClassLoader());
                }

                public final Creator<User> CREATOR = new Creator<User>() {
                    public User createFromParcel(Parcel source) {
                        return new User(source);
                    }

                    public User[] newArray(int size) {
                        return new User[size];
                    }
                };
            }

            public class Repo implements Parcelable {

                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("owner")
                @Expose
                private Owner owner;
                @SerializedName("name")
                @Expose
                private String name;
                @SerializedName("full_name")
                @Expose
                private String fullName;
                @SerializedName("description")
                @Expose
                private String description;
                @SerializedName("private")
                @Expose
                private Boolean _private;
                @SerializedName("fork")
                @Expose
                private Boolean fork;
                @SerializedName("url")
                @Expose
                private String url;
                @SerializedName("html_url")
                @Expose
                private String htmlUrl;
                @SerializedName("archive_url")
                @Expose
                private String archiveUrl;
                @SerializedName("assignees_url")
                @Expose
                private String assigneesUrl;
                @SerializedName("blobs_url")
                @Expose
                private String blobsUrl;
                @SerializedName("branches_url")
                @Expose
                private String branchesUrl;
                @SerializedName("clone_url")
                @Expose
                private String cloneUrl;
                @SerializedName("collaborators_url")
                @Expose
                private String collaboratorsUrl;
                @SerializedName("comments_url")
                @Expose
                private String commentsUrl;
                @SerializedName("commits_url")
                @Expose
                private String commitsUrl;
                @SerializedName("compare_url")
                @Expose
                private String compareUrl;
                @SerializedName("contents_url")
                @Expose
                private String contentsUrl;
                @SerializedName("contributors_url")
                @Expose
                private String contributorsUrl;
                @SerializedName("downloads_url")
                @Expose
                private String downloadsUrl;
                @SerializedName("events_url")
                @Expose
                private String eventsUrl;
                @SerializedName("forks_url")
                @Expose
                private String forksUrl;
                @SerializedName("git_commits_url")
                @Expose
                private String gitCommitsUrl;
                @SerializedName("git_refs_url")
                @Expose
                private String gitRefsUrl;
                @SerializedName("git_tags_url")
                @Expose
                private String gitTagsUrl;
                @SerializedName("git_url")
                @Expose
                private String gitUrl;
                @SerializedName("hooks_url")
                @Expose
                private String hooksUrl;
                @SerializedName("issue_comment_url")
                @Expose
                private String issueCommentUrl;
                @SerializedName("issue_events_url")
                @Expose
                private String issueEventsUrl;
                @SerializedName("issues_url")
                @Expose
                private String issuesUrl;
                @SerializedName("keys_url")
                @Expose
                private String keysUrl;
                @SerializedName("labels_url")
                @Expose
                private String labelsUrl;
                @SerializedName("languages_url")
                @Expose
                private String languagesUrl;
                @SerializedName("merges_url")
                @Expose
                private String mergesUrl;
                @SerializedName("milestones_url")
                @Expose
                private String milestonesUrl;
                @SerializedName("mirror_url")
                @Expose
                private String mirrorUrl;
                @SerializedName("notifications_url")
                @Expose
                private String notificationsUrl;
                @SerializedName("pulls_url")
                @Expose
                private String pullsUrl;
                @SerializedName("releases_url")
                @Expose
                private String releasesUrl;
                @SerializedName("ssh_url")
                @Expose
                private String sshUrl;
                @SerializedName("stargazers_url")
                @Expose
                private String stargazersUrl;
                @SerializedName("statuses_url")
                @Expose
                private String statusesUrl;
                @SerializedName("subscribers_url")
                @Expose
                private String subscribersUrl;
                @SerializedName("subscription_url")
                @Expose
                private String subscriptionUrl;
                @SerializedName("svn_url")
                @Expose
                private String svnUrl;
                @SerializedName("tags_url")
                @Expose
                private String tagsUrl;
                @SerializedName("teams_url")
                @Expose
                private String teamsUrl;
                @SerializedName("trees_url")
                @Expose
                private String treesUrl;
                @SerializedName("homepage")
                @Expose
                private String homepage;
                @SerializedName("language")
                @Expose
                private Object language;
                @SerializedName("forks_count")
                @Expose
                private Integer forksCount;
                @SerializedName("stargazers_count")
                @Expose
                private Integer stargazersCount;
                @SerializedName("watchers_count")
                @Expose
                private Integer watchersCount;
                @SerializedName("size")
                @Expose
                private Integer size;
                @SerializedName("default_branch")
                @Expose
                private String defaultBranch;
                @SerializedName("open_issues_count")
                @Expose
                private Integer openIssuesCount;
                @SerializedName("has_issues")
                @Expose
                private Boolean hasIssues;
                @SerializedName("has_wiki")
                @Expose
                private Boolean hasWiki;
                @SerializedName("has_pages")
                @Expose
                private Boolean hasPages;
                @SerializedName("has_downloads")
                @Expose
                private Boolean hasDownloads;
                @SerializedName("pushed_at")
                @Expose
                private String pushedAt;
                @SerializedName("created_at")
                @Expose
                private String createdAt;
                @SerializedName("updated_at")
                @Expose
                private String updatedAt;
                @SerializedName("permissions")
                @Expose
                private Permissions permissions;

                /**
                 *
                 * @return
                 * The id
                 */
                public Integer getId() {
                    return id;
                }

                /**
                 *
                 * @param id
                 * The id
                 */
                public void setId(Integer id) {
                    this.id = id;
                }

                /**
                 *
                 * @return
                 * The owner
                 */
                public Owner getOwner() {
                    return owner;
                }

                /**
                 *
                 * @param owner
                 * The owner
                 */
                public void setOwner(Owner owner) {
                    this.owner = owner;
                }

                /**
                 *
                 * @return
                 * The name
                 */
                public String getName() {
                    return name;
                }

                /**
                 *
                 * @param name
                 * The name
                 */
                public void setName(String name) {
                    this.name = name;
                }

                /**
                 *
                 * @return
                 * The fullName
                 */
                public String getFullName() {
                    return fullName;
                }

                /**
                 *
                 * @param fullName
                 * The full_name
                 */
                public void setFullName(String fullName) {
                    this.fullName = fullName;
                }

                /**
                 *
                 * @return
                 * The description
                 */
                public String getDescription() {
                    return description;
                }

                /**
                 *
                 * @param description
                 * The description
                 */
                public void setDescription(String description) {
                    this.description = description;
                }

                /**
                 *
                 * @return
                 * The _private
                 */
                public Boolean getPrivate() {
                    return _private;
                }

                /**
                 *
                 * @param _private
                 * The private
                 */
                public void setPrivate(Boolean _private) {
                    this._private = _private;
                }

                /**
                 *
                 * @return
                 * The fork
                 */
                public Boolean getFork() {
                    return fork;
                }

                /**
                 *
                 * @param fork
                 * The fork
                 */
                public void setFork(Boolean fork) {
                    this.fork = fork;
                }

                /**
                 *
                 * @return
                 * The url
                 */
                public String getUrl() {
                    return url;
                }

                /**
                 *
                 * @param url
                 * The url
                 */
                public void setUrl(String url) {
                    this.url = url;
                }

                /**
                 *
                 * @return
                 * The htmlUrl
                 */
                public String getHtmlUrl() {
                    return htmlUrl;
                }

                /**
                 *
                 * @param htmlUrl
                 * The html_url
                 */
                public void setHtmlUrl(String htmlUrl) {
                    this.htmlUrl = htmlUrl;
                }

                /**
                 *
                 * @return
                 * The archiveUrl
                 */
                public String getArchiveUrl() {
                    return archiveUrl;
                }

                /**
                 *
                 * @param archiveUrl
                 * The archive_url
                 */
                public void setArchiveUrl(String archiveUrl) {
                    this.archiveUrl = archiveUrl;
                }

                /**
                 *
                 * @return
                 * The assigneesUrl
                 */
                public String getAssigneesUrl() {
                    return assigneesUrl;
                }

                /**
                 *
                 * @param assigneesUrl
                 * The assignees_url
                 */
                public void setAssigneesUrl(String assigneesUrl) {
                    this.assigneesUrl = assigneesUrl;
                }

                /**
                 *
                 * @return
                 * The blobsUrl
                 */
                public String getBlobsUrl() {
                    return blobsUrl;
                }

                /**
                 *
                 * @param blobsUrl
                 * The blobs_url
                 */
                public void setBlobsUrl(String blobsUrl) {
                    this.blobsUrl = blobsUrl;
                }

                /**
                 *
                 * @return
                 * The branchesUrl
                 */
                public String getBranchesUrl() {
                    return branchesUrl;
                }

                /**
                 *
                 * @param branchesUrl
                 * The branches_url
                 */
                public void setBranchesUrl(String branchesUrl) {
                    this.branchesUrl = branchesUrl;
                }

                /**
                 *
                 * @return
                 * The cloneUrl
                 */
                public String getCloneUrl() {
                    return cloneUrl;
                }

                /**
                 *
                 * @param cloneUrl
                 * The clone_url
                 */
                public void setCloneUrl(String cloneUrl) {
                    this.cloneUrl = cloneUrl;
                }

                /**
                 *
                 * @return
                 * The collaboratorsUrl
                 */
                public String getCollaboratorsUrl() {
                    return collaboratorsUrl;
                }

                /**
                 *
                 * @param collaboratorsUrl
                 * The collaborators_url
                 */
                public void setCollaboratorsUrl(String collaboratorsUrl) {
                    this.collaboratorsUrl = collaboratorsUrl;
                }

                /**
                 *
                 * @return
                 * The commentsUrl
                 */
                public String getCommentsUrl() {
                    return commentsUrl;
                }

                /**
                 *
                 * @param commentsUrl
                 * The comments_url
                 */
                public void setCommentsUrl(String commentsUrl) {
                    this.commentsUrl = commentsUrl;
                }

                /**
                 *
                 * @return
                 * The commitsUrl
                 */
                public String getCommitsUrl() {
                    return commitsUrl;
                }

                /**
                 *
                 * @param commitsUrl
                 * The commits_url
                 */
                public void setCommitsUrl(String commitsUrl) {
                    this.commitsUrl = commitsUrl;
                }

                /**
                 *
                 * @return
                 * The compareUrl
                 */
                public String getCompareUrl() {
                    return compareUrl;
                }

                /**
                 *
                 * @param compareUrl
                 * The compare_url
                 */
                public void setCompareUrl(String compareUrl) {
                    this.compareUrl = compareUrl;
                }

                /**
                 *
                 * @return
                 * The contentsUrl
                 */
                public String getContentsUrl() {
                    return contentsUrl;
                }

                /**
                 *
                 * @param contentsUrl
                 * The contents_url
                 */
                public void setContentsUrl(String contentsUrl) {
                    this.contentsUrl = contentsUrl;
                }

                /**
                 *
                 * @return
                 * The contributorsUrl
                 */
                public String getContributorsUrl() {
                    return contributorsUrl;
                }

                /**
                 *
                 * @param contributorsUrl
                 * The contributors_url
                 */
                public void setContributorsUrl(String contributorsUrl) {
                    this.contributorsUrl = contributorsUrl;
                }

                /**
                 *
                 * @return
                 * The downloadsUrl
                 */
                public String getDownloadsUrl() {
                    return downloadsUrl;
                }

                /**
                 *
                 * @param downloadsUrl
                 * The downloads_url
                 */
                public void setDownloadsUrl(String downloadsUrl) {
                    this.downloadsUrl = downloadsUrl;
                }

                /**
                 *
                 * @return
                 * The eventsUrl
                 */
                public String getEventsUrl() {
                    return eventsUrl;
                }

                /**
                 *
                 * @param eventsUrl
                 * The events_url
                 */
                public void setEventsUrl(String eventsUrl) {
                    this.eventsUrl = eventsUrl;
                }

                /**
                 *
                 * @return
                 * The forksUrl
                 */
                public String getForksUrl() {
                    return forksUrl;
                }

                /**
                 *
                 * @param forksUrl
                 * The forks_url
                 */
                public void setForksUrl(String forksUrl) {
                    this.forksUrl = forksUrl;
                }

                /**
                 *
                 * @return
                 * The gitCommitsUrl
                 */
                public String getGitCommitsUrl() {
                    return gitCommitsUrl;
                }

                /**
                 *
                 * @param gitCommitsUrl
                 * The git_commits_url
                 */
                public void setGitCommitsUrl(String gitCommitsUrl) {
                    this.gitCommitsUrl = gitCommitsUrl;
                }

                /**
                 *
                 * @return
                 * The gitRefsUrl
                 */
                public String getGitRefsUrl() {
                    return gitRefsUrl;
                }

                /**
                 *
                 * @param gitRefsUrl
                 * The git_refs_url
                 */
                public void setGitRefsUrl(String gitRefsUrl) {
                    this.gitRefsUrl = gitRefsUrl;
                }

                /**
                 *
                 * @return
                 * The gitTagsUrl
                 */
                public String getGitTagsUrl() {
                    return gitTagsUrl;
                }

                /**
                 *
                 * @param gitTagsUrl
                 * The git_tags_url
                 */
                public void setGitTagsUrl(String gitTagsUrl) {
                    this.gitTagsUrl = gitTagsUrl;
                }

                /**
                 *
                 * @return
                 * The gitUrl
                 */
                public String getGitUrl() {
                    return gitUrl;
                }

                /**
                 *
                 * @param gitUrl
                 * The git_url
                 */
                public void setGitUrl(String gitUrl) {
                    this.gitUrl = gitUrl;
                }

                /**
                 *
                 * @return
                 * The hooksUrl
                 */
                public String getHooksUrl() {
                    return hooksUrl;
                }

                /**
                 *
                 * @param hooksUrl
                 * The hooks_url
                 */
                public void setHooksUrl(String hooksUrl) {
                    this.hooksUrl = hooksUrl;
                }

                /**
                 *
                 * @return
                 * The issueCommentUrl
                 */
                public String getIssueCommentUrl() {
                    return issueCommentUrl;
                }

                /**
                 *
                 * @param issueCommentUrl
                 * The issue_comment_url
                 */
                public void setIssueCommentUrl(String issueCommentUrl) {
                    this.issueCommentUrl = issueCommentUrl;
                }

                /**
                 *
                 * @return
                 * The issueEventsUrl
                 */
                public String getIssueEventsUrl() {
                    return issueEventsUrl;
                }

                /**
                 *
                 * @param issueEventsUrl
                 * The issue_events_url
                 */
                public void setIssueEventsUrl(String issueEventsUrl) {
                    this.issueEventsUrl = issueEventsUrl;
                }

                /**
                 *
                 * @return
                 * The issuesUrl
                 */
                public String getIssuesUrl() {
                    return issuesUrl;
                }

                /**
                 *
                 * @param issuesUrl
                 * The issues_url
                 */
                public void setIssuesUrl(String issuesUrl) {
                    this.issuesUrl = issuesUrl;
                }

                /**
                 *
                 * @return
                 * The keysUrl
                 */
                public String getKeysUrl() {
                    return keysUrl;
                }

                /**
                 *
                 * @param keysUrl
                 * The keys_url
                 */
                public void setKeysUrl(String keysUrl) {
                    this.keysUrl = keysUrl;
                }

                /**
                 *
                 * @return
                 * The labelsUrl
                 */
                public String getLabelsUrl() {
                    return labelsUrl;
                }

                /**
                 *
                 * @param labelsUrl
                 * The labels_url
                 */
                public void setLabelsUrl(String labelsUrl) {
                    this.labelsUrl = labelsUrl;
                }

                /**
                 *
                 * @return
                 * The languagesUrl
                 */
                public String getLanguagesUrl() {
                    return languagesUrl;
                }

                /**
                 *
                 * @param languagesUrl
                 * The languages_url
                 */
                public void setLanguagesUrl(String languagesUrl) {
                    this.languagesUrl = languagesUrl;
                }

                /**
                 *
                 * @return
                 * The mergesUrl
                 */
                public String getMergesUrl() {
                    return mergesUrl;
                }

                /**
                 *
                 * @param mergesUrl
                 * The merges_url
                 */
                public void setMergesUrl(String mergesUrl) {
                    this.mergesUrl = mergesUrl;
                }

                /**
                 *
                 * @return
                 * The milestonesUrl
                 */
                public String getMilestonesUrl() {
                    return milestonesUrl;
                }

                /**
                 *
                 * @param milestonesUrl
                 * The milestones_url
                 */
                public void setMilestonesUrl(String milestonesUrl) {
                    this.milestonesUrl = milestonesUrl;
                }

                /**
                 *
                 * @return
                 * The mirrorUrl
                 */
                public String getMirrorUrl() {
                    return mirrorUrl;
                }

                /**
                 *
                 * @param mirrorUrl
                 * The mirror_url
                 */
                public void setMirrorUrl(String mirrorUrl) {
                    this.mirrorUrl = mirrorUrl;
                }

                /**
                 *
                 * @return
                 * The notificationsUrl
                 */
                public String getNotificationsUrl() {
                    return notificationsUrl;
                }

                /**
                 *
                 * @param notificationsUrl
                 * The notifications_url
                 */
                public void setNotificationsUrl(String notificationsUrl) {
                    this.notificationsUrl = notificationsUrl;
                }

                /**
                 *
                 * @return
                 * The pullsUrl
                 */
                public String getPullsUrl() {
                    return pullsUrl;
                }

                /**
                 *
                 * @param pullsUrl
                 * The pulls_url
                 */
                public void setPullsUrl(String pullsUrl) {
                    this.pullsUrl = pullsUrl;
                }

                /**
                 *
                 * @return
                 * The releasesUrl
                 */
                public String getReleasesUrl() {
                    return releasesUrl;
                }

                /**
                 *
                 * @param releasesUrl
                 * The releases_url
                 */
                public void setReleasesUrl(String releasesUrl) {
                    this.releasesUrl = releasesUrl;
                }

                /**
                 *
                 * @return
                 * The sshUrl
                 */
                public String getSshUrl() {
                    return sshUrl;
                }

                /**
                 *
                 * @param sshUrl
                 * The ssh_url
                 */
                public void setSshUrl(String sshUrl) {
                    this.sshUrl = sshUrl;
                }

                /**
                 *
                 * @return
                 * The stargazersUrl
                 */
                public String getStargazersUrl() {
                    return stargazersUrl;
                }

                /**
                 *
                 * @param stargazersUrl
                 * The stargazers_url
                 */
                public void setStargazersUrl(String stargazersUrl) {
                    this.stargazersUrl = stargazersUrl;
                }

                /**
                 *
                 * @return
                 * The statusesUrl
                 */
                public String getStatusesUrl() {
                    return statusesUrl;
                }

                /**
                 *
                 * @param statusesUrl
                 * The statuses_url
                 */
                public void setStatusesUrl(String statusesUrl) {
                    this.statusesUrl = statusesUrl;
                }

                /**
                 *
                 * @return
                 * The subscribersUrl
                 */
                public String getSubscribersUrl() {
                    return subscribersUrl;
                }

                /**
                 *
                 * @param subscribersUrl
                 * The subscribers_url
                 */
                public void setSubscribersUrl(String subscribersUrl) {
                    this.subscribersUrl = subscribersUrl;
                }

                /**
                 *
                 * @return
                 * The subscriptionUrl
                 */
                public String getSubscriptionUrl() {
                    return subscriptionUrl;
                }

                /**
                 *
                 * @param subscriptionUrl
                 * The subscription_url
                 */
                public void setSubscriptionUrl(String subscriptionUrl) {
                    this.subscriptionUrl = subscriptionUrl;
                }

                /**
                 *
                 * @return
                 * The svnUrl
                 */
                public String getSvnUrl() {
                    return svnUrl;
                }

                /**
                 *
                 * @param svnUrl
                 * The svn_url
                 */
                public void setSvnUrl(String svnUrl) {
                    this.svnUrl = svnUrl;
                }

                /**
                 *
                 * @return
                 * The tagsUrl
                 */
                public String getTagsUrl() {
                    return tagsUrl;
                }

                /**
                 *
                 * @param tagsUrl
                 * The tags_url
                 */
                public void setTagsUrl(String tagsUrl) {
                    this.tagsUrl = tagsUrl;
                }

                /**
                 *
                 * @return
                 * The teamsUrl
                 */
                public String getTeamsUrl() {
                    return teamsUrl;
                }

                /**
                 *
                 * @param teamsUrl
                 * The teams_url
                 */
                public void setTeamsUrl(String teamsUrl) {
                    this.teamsUrl = teamsUrl;
                }

                /**
                 *
                 * @return
                 * The treesUrl
                 */
                public String getTreesUrl() {
                    return treesUrl;
                }

                /**
                 *
                 * @param treesUrl
                 * The trees_url
                 */
                public void setTreesUrl(String treesUrl) {
                    this.treesUrl = treesUrl;
                }

                /**
                 *
                 * @return
                 * The homepage
                 */
                public String getHomepage() {
                    return homepage;
                }

                /**
                 *
                 * @param homepage
                 * The homepage
                 */
                public void setHomepage(String homepage) {
                    this.homepage = homepage;
                }

                /**
                 *
                 * @return
                 * The language
                 */
                public Object getLanguage() {
                    return language;
                }

                /**
                 *
                 * @param language
                 * The language
                 */
                public void setLanguage(Object language) {
                    this.language = language;
                }

                /**
                 *
                 * @return
                 * The forksCount
                 */
                public Integer getForksCount() {
                    return forksCount;
                }

                /**
                 *
                 * @param forksCount
                 * The forks_count
                 */
                public void setForksCount(Integer forksCount) {
                    this.forksCount = forksCount;
                }

                /**
                 *
                 * @return
                 * The stargazersCount
                 */
                public Integer getStargazersCount() {
                    return stargazersCount;
                }

                /**
                 *
                 * @param stargazersCount
                 * The stargazers_count
                 */
                public void setStargazersCount(Integer stargazersCount) {
                    this.stargazersCount = stargazersCount;
                }

                /**
                 *
                 * @return
                 * The watchersCount
                 */
                public Integer getWatchersCount() {
                    return watchersCount;
                }

                /**
                 *
                 * @param watchersCount
                 * The watchers_count
                 */
                public void setWatchersCount(Integer watchersCount) {
                    this.watchersCount = watchersCount;
                }

                /**
                 *
                 * @return
                 * The size
                 */
                public Integer getSize() {
                    return size;
                }

                /**
                 *
                 * @param size
                 * The size
                 */
                public void setSize(Integer size) {
                    this.size = size;
                }

                /**
                 *
                 * @return
                 * The defaultBranch
                 */
                public String getDefaultBranch() {
                    return defaultBranch;
                }

                /**
                 *
                 * @param defaultBranch
                 * The default_branch
                 */
                public void setDefaultBranch(String defaultBranch) {
                    this.defaultBranch = defaultBranch;
                }

                /**
                 *
                 * @return
                 * The openIssuesCount
                 */
                public Integer getOpenIssuesCount() {
                    return openIssuesCount;
                }

                /**
                 *
                 * @param openIssuesCount
                 * The open_issues_count
                 */
                public void setOpenIssuesCount(Integer openIssuesCount) {
                    this.openIssuesCount = openIssuesCount;
                }

                /**
                 *
                 * @return
                 * The hasIssues
                 */
                public Boolean getHasIssues() {
                    return hasIssues;
                }

                /**
                 *
                 * @param hasIssues
                 * The has_issues
                 */
                public void setHasIssues(Boolean hasIssues) {
                    this.hasIssues = hasIssues;
                }

                /**
                 *
                 * @return
                 * The hasWiki
                 */
                public Boolean getHasWiki() {
                    return hasWiki;
                }

                /**
                 *
                 * @param hasWiki
                 * The has_wiki
                 */
                public void setHasWiki(Boolean hasWiki) {
                    this.hasWiki = hasWiki;
                }

                /**
                 *
                 * @return
                 * The hasPages
                 */
                public Boolean getHasPages() {
                    return hasPages;
                }

                /**
                 *
                 * @param hasPages
                 * The has_pages
                 */
                public void setHasPages(Boolean hasPages) {
                    this.hasPages = hasPages;
                }

                /**
                 *
                 * @return
                 * The hasDownloads
                 */
                public Boolean getHasDownloads() {
                    return hasDownloads;
                }

                /**
                 *
                 * @param hasDownloads
                 * The has_downloads
                 */
                public void setHasDownloads(Boolean hasDownloads) {
                    this.hasDownloads = hasDownloads;
                }

                /**
                 *
                 * @return
                 * The pushedAt
                 */
                public String getPushedAt() {
                    return pushedAt;
                }

                /**
                 *
                 * @param pushedAt
                 * The pushed_at
                 */
                public void setPushedAt(String pushedAt) {
                    this.pushedAt = pushedAt;
                }

                /**
                 *
                 * @return
                 * The createdAt
                 */
                public String getCreatedAt() {
                    return createdAt;
                }

                /**
                 *
                 * @param createdAt
                 * The created_at
                 */
                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                /**
                 *
                 * @return
                 * The updatedAt
                 */
                public String getUpdatedAt() {
                    return updatedAt;
                }

                /**
                 *
                 * @param updatedAt
                 * The updated_at
                 */
                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }

                /**
                 *
                 * @return
                 * The permissions
                 */
                public Permissions getPermissions() {
                    return permissions;
                }

                /**
                 *
                 * @param permissions
                 * The permissions
                 */
                public void setPermissions(Permissions permissions) {
                    this.permissions = permissions;
                }

                public class Owner implements Parcelable {

                    @SerializedName("login")
                    @Expose
                    private String login;
                    @SerializedName("id")
                    @Expose
                    private Integer id;
                    @SerializedName("avatar_url")
                    @Expose
                    private String avatarUrl;
                    @SerializedName("gravatar_id")
                    @Expose
                    private String gravatarId;
                    @SerializedName("url")
                    @Expose
                    private String url;
                    @SerializedName("html_url")
                    @Expose
                    private String htmlUrl;
                    @SerializedName("followers_url")
                    @Expose
                    private String followersUrl;
                    @SerializedName("following_url")
                    @Expose
                    private String followingUrl;
                    @SerializedName("gists_url")
                    @Expose
                    private String gistsUrl;
                    @SerializedName("starred_url")
                    @Expose
                    private String starredUrl;
                    @SerializedName("subscriptions_url")
                    @Expose
                    private String subscriptionsUrl;
                    @SerializedName("organizations_url")
                    @Expose
                    private String organizationsUrl;
                    @SerializedName("repos_url")
                    @Expose
                    private String reposUrl;
                    @SerializedName("events_url")
                    @Expose
                    private String eventsUrl;
                    @SerializedName("received_events_url")
                    @Expose
                    private String receivedEventsUrl;
                    @SerializedName("type")
                    @Expose
                    private String type;
                    @SerializedName("site_admin")
                    @Expose
                    private Boolean siteAdmin;

                    /**
                     *
                     * @return
                     * The login
                     */
                    public String getLogin() {
                        return login;
                    }

                    /**
                     *
                     * @param login
                     * The login
                     */
                    public void setLogin(String login) {
                        this.login = login;
                    }

                    /**
                     *
                     * @return
                     * The id
                     */
                    public Integer getId() {
                        return id;
                    }

                    /**
                     *
                     * @param id
                     * The id
                     */
                    public void setId(Integer id) {
                        this.id = id;
                    }

                    /**
                     *
                     * @return
                     * The avatarUrl
                     */
                    public String getAvatarUrl() {
                        return avatarUrl;
                    }

                    /**
                     *
                     * @param avatarUrl
                     * The avatar_url
                     */
                    public void setAvatarUrl(String avatarUrl) {
                        this.avatarUrl = avatarUrl;
                    }

                    /**
                     *
                     * @return
                     * The gravatarId
                     */
                    public String getGravatarId() {
                        return gravatarId;
                    }

                    /**
                     *
                     * @param gravatarId
                     * The gravatar_id
                     */
                    public void setGravatarId(String gravatarId) {
                        this.gravatarId = gravatarId;
                    }

                    /**
                     *
                     * @return
                     * The url
                     */
                    public String getUrl() {
                        return url;
                    }

                    /**
                     *
                     * @param url
                     * The url
                     */
                    public void setUrl(String url) {
                        this.url = url;
                    }

                    /**
                     *
                     * @return
                     * The htmlUrl
                     */
                    public String getHtmlUrl() {
                        return htmlUrl;
                    }

                    /**
                     *
                     * @param htmlUrl
                     * The html_url
                     */
                    public void setHtmlUrl(String htmlUrl) {
                        this.htmlUrl = htmlUrl;
                    }

                    /**
                     *
                     * @return
                     * The followersUrl
                     */
                    public String getFollowersUrl() {
                        return followersUrl;
                    }

                    /**
                     *
                     * @param followersUrl
                     * The followers_url
                     */
                    public void setFollowersUrl(String followersUrl) {
                        this.followersUrl = followersUrl;
                    }

                    /**
                     *
                     * @return
                     * The followingUrl
                     */
                    public String getFollowingUrl() {
                        return followingUrl;
                    }

                    /**
                     *
                     * @param followingUrl
                     * The following_url
                     */
                    public void setFollowingUrl(String followingUrl) {
                        this.followingUrl = followingUrl;
                    }

                    /**
                     *
                     * @return
                     * The gistsUrl
                     */
                    public String getGistsUrl() {
                        return gistsUrl;
                    }

                    /**
                     *
                     * @param gistsUrl
                     * The gists_url
                     */
                    public void setGistsUrl(String gistsUrl) {
                        this.gistsUrl = gistsUrl;
                    }

                    /**
                     *
                     * @return
                     * The starredUrl
                     */
                    public String getStarredUrl() {
                        return starredUrl;
                    }

                    /**
                     *
                     * @param starredUrl
                     * The starred_url
                     */
                    public void setStarredUrl(String starredUrl) {
                        this.starredUrl = starredUrl;
                    }

                    /**
                     *
                     * @return
                     * The subscriptionsUrl
                     */
                    public String getSubscriptionsUrl() {
                        return subscriptionsUrl;
                    }

                    /**
                     *
                     * @param subscriptionsUrl
                     * The subscriptions_url
                     */
                    public void setSubscriptionsUrl(String subscriptionsUrl) {
                        this.subscriptionsUrl = subscriptionsUrl;
                    }

                    /**
                     *
                     * @return
                     * The organizationsUrl
                     */
                    public String getOrganizationsUrl() {
                        return organizationsUrl;
                    }

                    /**
                     *
                     * @param organizationsUrl
                     * The organizations_url
                     */
                    public void setOrganizationsUrl(String organizationsUrl) {
                        this.organizationsUrl = organizationsUrl;
                    }

                    /**
                     *
                     * @return
                     * The reposUrl
                     */
                    public String getReposUrl() {
                        return reposUrl;
                    }

                    /**
                     *
                     * @param reposUrl
                     * The repos_url
                     */
                    public void setReposUrl(String reposUrl) {
                        this.reposUrl = reposUrl;
                    }

                    /**
                     *
                     * @return
                     * The eventsUrl
                     */
                    public String getEventsUrl() {
                        return eventsUrl;
                    }

                    /**
                     *
                     * @param eventsUrl
                     * The events_url
                     */
                    public void setEventsUrl(String eventsUrl) {
                        this.eventsUrl = eventsUrl;
                    }

                    /**
                     *
                     * @return
                     * The receivedEventsUrl
                     */
                    public String getReceivedEventsUrl() {
                        return receivedEventsUrl;
                    }

                    /**
                     *
                     * @param receivedEventsUrl
                     * The received_events_url
                     */
                    public void setReceivedEventsUrl(String receivedEventsUrl) {
                        this.receivedEventsUrl = receivedEventsUrl;
                    }

                    /**
                     *
                     * @return
                     * The type
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     *
                     * @param type
                     * The type
                     */
                    public void setType(String type) {
                        this.type = type;
                    }

                    /**
                     *
                     * @return
                     * The siteAdmin
                     */
                    public Boolean getSiteAdmin() {
                        return siteAdmin;
                    }

                    /**
                     *
                     * @param siteAdmin
                     * The site_admin
                     */
                    public void setSiteAdmin(Boolean siteAdmin) {
                        this.siteAdmin = siteAdmin;
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeString(this.login);
                        dest.writeValue(this.id);
                        dest.writeString(this.avatarUrl);
                        dest.writeString(this.gravatarId);
                        dest.writeString(this.url);
                        dest.writeString(this.htmlUrl);
                        dest.writeString(this.followersUrl);
                        dest.writeString(this.followingUrl);
                        dest.writeString(this.gistsUrl);
                        dest.writeString(this.starredUrl);
                        dest.writeString(this.subscriptionsUrl);
                        dest.writeString(this.organizationsUrl);
                        dest.writeString(this.reposUrl);
                        dest.writeString(this.eventsUrl);
                        dest.writeString(this.receivedEventsUrl);
                        dest.writeString(this.type);
                        dest.writeValue(this.siteAdmin);
                    }

                    public Owner() {
                    }

                    protected Owner(Parcel in) {
                        this.login = in.readString();
                        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
                        this.avatarUrl = in.readString();
                        this.gravatarId = in.readString();
                        this.url = in.readString();
                        this.htmlUrl = in.readString();
                        this.followersUrl = in.readString();
                        this.followingUrl = in.readString();
                        this.gistsUrl = in.readString();
                        this.starredUrl = in.readString();
                        this.subscriptionsUrl = in.readString();
                        this.organizationsUrl = in.readString();
                        this.reposUrl = in.readString();
                        this.eventsUrl = in.readString();
                        this.receivedEventsUrl = in.readString();
                        this.type = in.readString();
                        this.siteAdmin = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    }

                    public final Creator<Owner> CREATOR = new Creator<Owner>() {
                        public Owner createFromParcel(Parcel source) {
                            return new Owner(source);
                        }

                        public Owner[] newArray(int size) {
                            return new Owner[size];
                        }
                    };
                }

                public class Permissions implements Parcelable {

                    @SerializedName("admin")
                    @Expose
                    private Boolean admin;
                    @SerializedName("push")
                    @Expose
                    private Boolean push;
                    @SerializedName("pull")
                    @Expose
                    private Boolean pull;

                    /**
                     *
                     * @return
                     * The admin
                     */
                    public Boolean getAdmin() {
                        return admin;
                    }

                    /**
                     *
                     * @param admin
                     * The admin
                     */
                    public void setAdmin(Boolean admin) {
                        this.admin = admin;
                    }

                    /**
                     *
                     * @return
                     * The push
                     */
                    public Boolean getPush() {
                        return push;
                    }

                    /**
                     *
                     * @param push
                     * The push
                     */
                    public void setPush(Boolean push) {
                        this.push = push;
                    }

                    /**
                     *
                     * @return
                     * The pull
                     */
                    public Boolean getPull() {
                        return pull;
                    }

                    /**
                     *
                     * @param pull
                     * The pull
                     */
                    public void setPull(Boolean pull) {
                        this.pull = pull;
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeValue(this.admin);
                        dest.writeValue(this.push);
                        dest.writeValue(this.pull);
                    }

                    public Permissions() {
                    }

                    protected Permissions(Parcel in) {
                        this.admin = (Boolean) in.readValue(Boolean.class.getClassLoader());
                        this.push = (Boolean) in.readValue(Boolean.class.getClassLoader());
                        this.pull = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    }

                    public final Creator<Permissions> CREATOR = new Creator<Permissions>() {
                        public Permissions createFromParcel(Parcel source) {
                            return new Permissions(source);
                        }

                        public Permissions[] newArray(int size) {
                            return new Permissions[size];
                        }
                    };
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeValue(this.id);
                    dest.writeParcelable(this.owner, flags);
                    dest.writeString(this.name);
                    dest.writeString(this.fullName);
                    dest.writeString(this.description);
                    dest.writeValue(this._private);
                    dest.writeValue(this.fork);
                    dest.writeString(this.url);
                    dest.writeString(this.htmlUrl);
                    dest.writeString(this.archiveUrl);
                    dest.writeString(this.assigneesUrl);
                    dest.writeString(this.blobsUrl);
                    dest.writeString(this.branchesUrl);
                    dest.writeString(this.cloneUrl);
                    dest.writeString(this.collaboratorsUrl);
                    dest.writeString(this.commentsUrl);
                    dest.writeString(this.commitsUrl);
                    dest.writeString(this.compareUrl);
                    dest.writeString(this.contentsUrl);
                    dest.writeString(this.contributorsUrl);
                    dest.writeString(this.downloadsUrl);
                    dest.writeString(this.eventsUrl);
                    dest.writeString(this.forksUrl);
                    dest.writeString(this.gitCommitsUrl);
                    dest.writeString(this.gitRefsUrl);
                    dest.writeString(this.gitTagsUrl);
                    dest.writeString(this.gitUrl);
                    dest.writeString(this.hooksUrl);
                    dest.writeString(this.issueCommentUrl);
                    dest.writeString(this.issueEventsUrl);
                    dest.writeString(this.issuesUrl);
                    dest.writeString(this.keysUrl);
                    dest.writeString(this.labelsUrl);
                    dest.writeString(this.languagesUrl);
                    dest.writeString(this.mergesUrl);
                    dest.writeString(this.milestonesUrl);
                    dest.writeString(this.mirrorUrl);
                    dest.writeString(this.notificationsUrl);
                    dest.writeString(this.pullsUrl);
                    dest.writeString(this.releasesUrl);
                    dest.writeString(this.sshUrl);
                    dest.writeString(this.stargazersUrl);
                    dest.writeString(this.statusesUrl);
                    dest.writeString(this.subscribersUrl);
                    dest.writeString(this.subscriptionUrl);
                    dest.writeString(this.svnUrl);
                    dest.writeString(this.tagsUrl);
                    dest.writeString(this.teamsUrl);
                    dest.writeString(this.treesUrl);
                    dest.writeString(this.homepage);
                    dest.writeValue(this.language);
                    dest.writeValue(this.forksCount);
                    dest.writeValue(this.stargazersCount);
                    dest.writeValue(this.watchersCount);
                    dest.writeValue(this.size);
                    dest.writeString(this.defaultBranch);
                    dest.writeValue(this.openIssuesCount);
                    dest.writeValue(this.hasIssues);
                    dest.writeValue(this.hasWiki);
                    dest.writeValue(this.hasPages);
                    dest.writeValue(this.hasDownloads);
                    dest.writeString(this.pushedAt);
                    dest.writeString(this.createdAt);
                    dest.writeString(this.updatedAt);
                    dest.writeParcelable(this.permissions, flags);
                }

                public Repo() {
                }

                protected Repo(Parcel in) {
                    this.id = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.owner = in.readParcelable(Owner.class.getClassLoader());
                    this.name = in.readString();
                    this.fullName = in.readString();
                    this.description = in.readString();
                    this._private = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.fork = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.url = in.readString();
                    this.htmlUrl = in.readString();
                    this.archiveUrl = in.readString();
                    this.assigneesUrl = in.readString();
                    this.blobsUrl = in.readString();
                    this.branchesUrl = in.readString();
                    this.cloneUrl = in.readString();
                    this.collaboratorsUrl = in.readString();
                    this.commentsUrl = in.readString();
                    this.commitsUrl = in.readString();
                    this.compareUrl = in.readString();
                    this.contentsUrl = in.readString();
                    this.contributorsUrl = in.readString();
                    this.downloadsUrl = in.readString();
                    this.eventsUrl = in.readString();
                    this.forksUrl = in.readString();
                    this.gitCommitsUrl = in.readString();
                    this.gitRefsUrl = in.readString();
                    this.gitTagsUrl = in.readString();
                    this.gitUrl = in.readString();
                    this.hooksUrl = in.readString();
                    this.issueCommentUrl = in.readString();
                    this.issueEventsUrl = in.readString();
                    this.issuesUrl = in.readString();
                    this.keysUrl = in.readString();
                    this.labelsUrl = in.readString();
                    this.languagesUrl = in.readString();
                    this.mergesUrl = in.readString();
                    this.milestonesUrl = in.readString();
                    this.mirrorUrl = in.readString();
                    this.notificationsUrl = in.readString();
                    this.pullsUrl = in.readString();
                    this.releasesUrl = in.readString();
                    this.sshUrl = in.readString();
                    this.stargazersUrl = in.readString();
                    this.statusesUrl = in.readString();
                    this.subscribersUrl = in.readString();
                    this.subscriptionUrl = in.readString();
                    this.svnUrl = in.readString();
                    this.tagsUrl = in.readString();
                    this.teamsUrl = in.readString();
                    this.treesUrl = in.readString();
                    this.homepage = in.readString();
                    this.language = in.readParcelable(Object.class.getClassLoader());
                    this.forksCount = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.stargazersCount = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.watchersCount = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.size = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.defaultBranch = in.readString();
                    this.openIssuesCount = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.hasIssues = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.hasWiki = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.hasPages = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.hasDownloads = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.pushedAt = in.readString();
                    this.createdAt = in.readString();
                    this.updatedAt = in.readString();
                    this.permissions = in.readParcelable(Permissions.class.getClassLoader());
                }

                public final Creator<Repo> CREATOR = new Creator<Repo>() {
                    public Repo createFromParcel(Parcel source) {
                        return new Repo(source);
                    }

                    public Repo[] newArray(int size) {
                        return new Repo[size];
                    }
                };
            }


            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.label);
                dest.writeString(this.ref);
                dest.writeString(this.sha);
                dest.writeParcelable(this.user, flags);
                dest.writeParcelable(this.repo, flags);
            }

            public Head() {
            }

            protected Head(Parcel in) {
                this.label = in.readString();
                this.ref = in.readString();
                this.sha = in.readString();
                this.user = in.readParcelable(User.class.getClassLoader());
                this.repo = in.readParcelable(Repo.class.getClassLoader());
            }

            public final Creator<Head> CREATOR = new Creator<Head>() {
                public Head createFromParcel(Parcel source) {
                    return new Head(source);
                }

                public Head[] newArray(int size) {
                    return new Head[size];
                }
            };
        }

        public class Base implements Parcelable {

            @SerializedName("label")
            @Expose
            private String label;
            @SerializedName("ref")
            @Expose
            private String ref;
            @SerializedName("sha")
            @Expose
            private String sha;
            @SerializedName("user")
            @Expose
            private User_ user;
            @SerializedName("repo")
            @Expose
            private Repo_ repo;

            /**
             *
             * @return
             * The label
             */
            public String getLabel() {
                return label;
            }

            /**
             *
             * @param label
             * The label
             */
            public void setLabel(String label) {
                this.label = label;
            }

            /**
             *
             * @return
             * The ref
             */
            public String getRef() {
                return ref;
            }

            /**
             *
             * @param ref
             * The ref
             */
            public void setRef(String ref) {
                this.ref = ref;
            }

            /**
             *
             * @return
             * The sha
             */
            public String getSha() {
                return sha;
            }

            /**
             *
             * @param sha
             * The sha
             */
            public void setSha(String sha) {
                this.sha = sha;
            }

            /**
             *
             * @return
             * The user
             */
            public User_ getUser() {
                return user;
            }

            /**
             *
             * @param user
             * The user
             */
            public void setUser(User_ user) {
                this.user = user;
            }

            /**
             *
             * @return
             * The repo
             */
            public Repo_ getRepo() {
                return repo;
            }

            /**
             *
             * @param repo
             * The repo
             */
            public void setRepo(Repo_ repo) {
                this.repo = repo;
            }

            public class User_ implements Parcelable {

                @SerializedName("login")
                @Expose
                private String login;
                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("avatar_url")
                @Expose
                private String avatarUrl;
                @SerializedName("gravatar_id")
                @Expose
                private String gravatarId;
                @SerializedName("url")
                @Expose
                private String url;
                @SerializedName("html_url")
                @Expose
                private String htmlUrl;
                @SerializedName("followers_url")
                @Expose
                private String followersUrl;
                @SerializedName("following_url")
                @Expose
                private String followingUrl;
                @SerializedName("gists_url")
                @Expose
                private String gistsUrl;
                @SerializedName("starred_url")
                @Expose
                private String starredUrl;
                @SerializedName("subscriptions_url")
                @Expose
                private String subscriptionsUrl;
                @SerializedName("organizations_url")
                @Expose
                private String organizationsUrl;
                @SerializedName("repos_url")
                @Expose
                private String reposUrl;
                @SerializedName("events_url")
                @Expose
                private String eventsUrl;
                @SerializedName("received_events_url")
                @Expose
                private String receivedEventsUrl;
                @SerializedName("type")
                @Expose
                private String type;
                @SerializedName("site_admin")
                @Expose
                private Boolean siteAdmin;

                /**
                 *
                 * @return
                 * The login
                 */
                public String getLogin() {
                    return login;
                }

                /**
                 *
                 * @param login
                 * The login
                 */
                public void setLogin(String login) {
                    this.login = login;
                }

                /**
                 *
                 * @return
                 * The id
                 */
                public Integer getId() {
                    return id;
                }

                /**
                 *
                 * @param id
                 * The id
                 */
                public void setId(Integer id) {
                    this.id = id;
                }

                /**
                 *
                 * @return
                 * The avatarUrl
                 */
                public String getAvatarUrl() {
                    return avatarUrl;
                }

                /**
                 *
                 * @param avatarUrl
                 * The avatar_url
                 */
                public void setAvatarUrl(String avatarUrl) {
                    this.avatarUrl = avatarUrl;
                }

                /**
                 *
                 * @return
                 * The gravatarId
                 */
                public String getGravatarId() {
                    return gravatarId;
                }

                /**
                 *
                 * @param gravatarId
                 * The gravatar_id
                 */
                public void setGravatarId(String gravatarId) {
                    this.gravatarId = gravatarId;
                }

                /**
                 *
                 * @return
                 * The url
                 */
                public String getUrl() {
                    return url;
                }

                /**
                 *
                 * @param url
                 * The url
                 */
                public void setUrl(String url) {
                    this.url = url;
                }

                /**
                 *
                 * @return
                 * The htmlUrl
                 */
                public String getHtmlUrl() {
                    return htmlUrl;
                }

                /**
                 *
                 * @param htmlUrl
                 * The html_url
                 */
                public void setHtmlUrl(String htmlUrl) {
                    this.htmlUrl = htmlUrl;
                }

                /**
                 *
                 * @return
                 * The followersUrl
                 */
                public String getFollowersUrl() {
                    return followersUrl;
                }

                /**
                 *
                 * @param followersUrl
                 * The followers_url
                 */
                public void setFollowersUrl(String followersUrl) {
                    this.followersUrl = followersUrl;
                }

                /**
                 *
                 * @return
                 * The followingUrl
                 */
                public String getFollowingUrl() {
                    return followingUrl;
                }

                /**
                 *
                 * @param followingUrl
                 * The following_url
                 */
                public void setFollowingUrl(String followingUrl) {
                    this.followingUrl = followingUrl;
                }

                /**
                 *
                 * @return
                 * The gistsUrl
                 */
                public String getGistsUrl() {
                    return gistsUrl;
                }

                /**
                 *
                 * @param gistsUrl
                 * The gists_url
                 */
                public void setGistsUrl(String gistsUrl) {
                    this.gistsUrl = gistsUrl;
                }

                /**
                 *
                 * @return
                 * The starredUrl
                 */
                public String getStarredUrl() {
                    return starredUrl;
                }

                /**
                 *
                 * @param starredUrl
                 * The starred_url
                 */
                public void setStarredUrl(String starredUrl) {
                    this.starredUrl = starredUrl;
                }

                /**
                 *
                 * @return
                 * The subscriptionsUrl
                 */
                public String getSubscriptionsUrl() {
                    return subscriptionsUrl;
                }

                /**
                 *
                 * @param subscriptionsUrl
                 * The subscriptions_url
                 */
                public void setSubscriptionsUrl(String subscriptionsUrl) {
                    this.subscriptionsUrl = subscriptionsUrl;
                }

                /**
                 *
                 * @return
                 * The organizationsUrl
                 */
                public String getOrganizationsUrl() {
                    return organizationsUrl;
                }

                /**
                 *
                 * @param organizationsUrl
                 * The organizations_url
                 */
                public void setOrganizationsUrl(String organizationsUrl) {
                    this.organizationsUrl = organizationsUrl;
                }

                /**
                 *
                 * @return
                 * The reposUrl
                 */
                public String getReposUrl() {
                    return reposUrl;
                }

                /**
                 *
                 * @param reposUrl
                 * The repos_url
                 */
                public void setReposUrl(String reposUrl) {
                    this.reposUrl = reposUrl;
                }

                /**
                 *
                 * @return
                 * The eventsUrl
                 */
                public String getEventsUrl() {
                    return eventsUrl;
                }

                /**
                 *
                 * @param eventsUrl
                 * The events_url
                 */
                public void setEventsUrl(String eventsUrl) {
                    this.eventsUrl = eventsUrl;
                }

                /**
                 *
                 * @return
                 * The receivedEventsUrl
                 */
                public String getReceivedEventsUrl() {
                    return receivedEventsUrl;
                }

                /**
                 *
                 * @param receivedEventsUrl
                 * The received_events_url
                 */
                public void setReceivedEventsUrl(String receivedEventsUrl) {
                    this.receivedEventsUrl = receivedEventsUrl;
                }

                /**
                 *
                 * @return
                 * The type
                 */
                public String getType() {
                    return type;
                }

                /**
                 *
                 * @param type
                 * The type
                 */
                public void setType(String type) {
                    this.type = type;
                }

                /**
                 *
                 * @return
                 * The siteAdmin
                 */
                public Boolean getSiteAdmin() {
                    return siteAdmin;
                }

                /**
                 *
                 * @param siteAdmin
                 * The site_admin
                 */
                public void setSiteAdmin(Boolean siteAdmin) {
                    this.siteAdmin = siteAdmin;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.login);
                    dest.writeValue(this.id);
                    dest.writeString(this.avatarUrl);
                    dest.writeString(this.gravatarId);
                    dest.writeString(this.url);
                    dest.writeString(this.htmlUrl);
                    dest.writeString(this.followersUrl);
                    dest.writeString(this.followingUrl);
                    dest.writeString(this.gistsUrl);
                    dest.writeString(this.starredUrl);
                    dest.writeString(this.subscriptionsUrl);
                    dest.writeString(this.organizationsUrl);
                    dest.writeString(this.reposUrl);
                    dest.writeString(this.eventsUrl);
                    dest.writeString(this.receivedEventsUrl);
                    dest.writeString(this.type);
                    dest.writeValue(this.siteAdmin);
                }

                public User_() {
                }

                protected User_(Parcel in) {
                    this.login = in.readString();
                    this.id = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.avatarUrl = in.readString();
                    this.gravatarId = in.readString();
                    this.url = in.readString();
                    this.htmlUrl = in.readString();
                    this.followersUrl = in.readString();
                    this.followingUrl = in.readString();
                    this.gistsUrl = in.readString();
                    this.starredUrl = in.readString();
                    this.subscriptionsUrl = in.readString();
                    this.organizationsUrl = in.readString();
                    this.reposUrl = in.readString();
                    this.eventsUrl = in.readString();
                    this.receivedEventsUrl = in.readString();
                    this.type = in.readString();
                    this.siteAdmin = (Boolean) in.readValue(Boolean.class.getClassLoader());
                }

                public final Creator<User_> CREATOR = new Creator<User_>() {
                    public User_ createFromParcel(Parcel source) {
                        return new User_(source);
                    }

                    public User_[] newArray(int size) {
                        return new User_[size];
                    }
                };
            }

            public class Repo_ implements Parcelable {

                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("owner")
                @Expose
                private Owner_ owner;
                @SerializedName("name")
                @Expose
                private String name;
                @SerializedName("full_name")
                @Expose
                private String fullName;
                @SerializedName("description")
                @Expose
                private String description;
                @SerializedName("private")
                @Expose
                private Boolean _private;
                @SerializedName("fork")
                @Expose
                private Boolean fork;
                @SerializedName("url")
                @Expose
                private String url;
                @SerializedName("html_url")
                @Expose
                private String htmlUrl;
                @SerializedName("archive_url")
                @Expose
                private String archiveUrl;
                @SerializedName("assignees_url")
                @Expose
                private String assigneesUrl;
                @SerializedName("blobs_url")
                @Expose
                private String blobsUrl;
                @SerializedName("branches_url")
                @Expose
                private String branchesUrl;
                @SerializedName("clone_url")
                @Expose
                private String cloneUrl;
                @SerializedName("collaborators_url")
                @Expose
                private String collaboratorsUrl;
                @SerializedName("comments_url")
                @Expose
                private String commentsUrl;
                @SerializedName("commits_url")
                @Expose
                private String commitsUrl;
                @SerializedName("compare_url")
                @Expose
                private String compareUrl;
                @SerializedName("contents_url")
                @Expose
                private String contentsUrl;
                @SerializedName("contributors_url")
                @Expose
                private String contributorsUrl;
                @SerializedName("downloads_url")
                @Expose
                private String downloadsUrl;
                @SerializedName("events_url")
                @Expose
                private String eventsUrl;
                @SerializedName("forks_url")
                @Expose
                private String forksUrl;
                @SerializedName("git_commits_url")
                @Expose
                private String gitCommitsUrl;
                @SerializedName("git_refs_url")
                @Expose
                private String gitRefsUrl;
                @SerializedName("git_tags_url")
                @Expose
                private String gitTagsUrl;
                @SerializedName("git_url")
                @Expose
                private String gitUrl;
                @SerializedName("hooks_url")
                @Expose
                private String hooksUrl;
                @SerializedName("issue_comment_url")
                @Expose
                private String issueCommentUrl;
                @SerializedName("issue_events_url")
                @Expose
                private String issueEventsUrl;
                @SerializedName("issues_url")
                @Expose
                private String issuesUrl;
                @SerializedName("keys_url")
                @Expose
                private String keysUrl;
                @SerializedName("labels_url")
                @Expose
                private String labelsUrl;
                @SerializedName("languages_url")
                @Expose
                private String languagesUrl;
                @SerializedName("merges_url")
                @Expose
                private String mergesUrl;
                @SerializedName("milestones_url")
                @Expose
                private String milestonesUrl;
                @SerializedName("mirror_url")
                @Expose
                private String mirrorUrl;
                @SerializedName("notifications_url")
                @Expose
                private String notificationsUrl;
                @SerializedName("pulls_url")
                @Expose
                private String pullsUrl;
                @SerializedName("releases_url")
                @Expose
                private String releasesUrl;
                @SerializedName("ssh_url")
                @Expose
                private String sshUrl;
                @SerializedName("stargazers_url")
                @Expose
                private String stargazersUrl;
                @SerializedName("statuses_url")
                @Expose
                private String statusesUrl;
                @SerializedName("subscribers_url")
                @Expose
                private String subscribersUrl;
                @SerializedName("subscription_url")
                @Expose
                private String subscriptionUrl;
                @SerializedName("svn_url")
                @Expose
                private String svnUrl;
                @SerializedName("tags_url")
                @Expose
                private String tagsUrl;
                @SerializedName("teams_url")
                @Expose
                private String teamsUrl;
                @SerializedName("trees_url")
                @Expose
                private String treesUrl;
                @SerializedName("homepage")
                @Expose
                private String homepage;
                @SerializedName("language")
                @Expose
                private Object language;
                @SerializedName("forks_count")
                @Expose
                private Integer forksCount;
                @SerializedName("stargazers_count")
                @Expose
                private Integer stargazersCount;
                @SerializedName("watchers_count")
                @Expose
                private Integer watchersCount;
                @SerializedName("size")
                @Expose
                private Integer size;
                @SerializedName("default_branch")
                @Expose
                private String defaultBranch;
                @SerializedName("open_issues_count")
                @Expose
                private Integer openIssuesCount;
                @SerializedName("has_issues")
                @Expose
                private Boolean hasIssues;
                @SerializedName("has_wiki")
                @Expose
                private Boolean hasWiki;
                @SerializedName("has_pages")
                @Expose
                private Boolean hasPages;
                @SerializedName("has_downloads")
                @Expose
                private Boolean hasDownloads;
                @SerializedName("pushed_at")
                @Expose
                private String pushedAt;
                @SerializedName("created_at")
                @Expose
                private String createdAt;
                @SerializedName("updated_at")
                @Expose
                private String updatedAt;
                @SerializedName("permissions")
                @Expose
                private Permissions_ permissions;

                /**
                 *
                 * @return
                 * The id
                 */
                public Integer getId() {
                    return id;
                }

                /**
                 *
                 * @param id
                 * The id
                 */
                public void setId(Integer id) {
                    this.id = id;
                }

                /**
                 *
                 * @return
                 * The owner
                 */
                public Owner_ getOwner() {
                    return owner;
                }

                /**
                 *
                 * @param owner
                 * The owner
                 */
                public void setOwner(Owner_ owner) {
                    this.owner = owner;
                }

                /**
                 *
                 * @return
                 * The name
                 */
                public String getName() {
                    return name;
                }

                /**
                 *
                 * @param name
                 * The name
                 */
                public void setName(String name) {
                    this.name = name;
                }

                /**
                 *
                 * @return
                 * The fullName
                 */
                public String getFullName() {
                    return fullName;
                }

                /**
                 *
                 * @param fullName
                 * The full_name
                 */
                public void setFullName(String fullName) {
                    this.fullName = fullName;
                }

                /**
                 *
                 * @return
                 * The description
                 */
                public String getDescription() {
                    return description;
                }

                /**
                 *
                 * @param description
                 * The description
                 */
                public void setDescription(String description) {
                    this.description = description;
                }

                /**
                 *
                 * @return
                 * The _private
                 */
                public Boolean getPrivate() {
                    return _private;
                }

                /**
                 *
                 * @param _private
                 * The private
                 */
                public void setPrivate(Boolean _private) {
                    this._private = _private;
                }

                /**
                 *
                 * @return
                 * The fork
                 */
                public Boolean getFork() {
                    return fork;
                }

                /**
                 *
                 * @param fork
                 * The fork
                 */
                public void setFork(Boolean fork) {
                    this.fork = fork;
                }

                /**
                 *
                 * @return
                 * The url
                 */
                public String getUrl() {
                    return url;
                }

                /**
                 *
                 * @param url
                 * The url
                 */
                public void setUrl(String url) {
                    this.url = url;
                }

                /**
                 *
                 * @return
                 * The htmlUrl
                 */
                public String getHtmlUrl() {
                    return htmlUrl;
                }

                /**
                 *
                 * @param htmlUrl
                 * The html_url
                 */
                public void setHtmlUrl(String htmlUrl) {
                    this.htmlUrl = htmlUrl;
                }

                /**
                 *
                 * @return
                 * The archiveUrl
                 */
                public String getArchiveUrl() {
                    return archiveUrl;
                }

                /**
                 *
                 * @param archiveUrl
                 * The archive_url
                 */
                public void setArchiveUrl(String archiveUrl) {
                    this.archiveUrl = archiveUrl;
                }

                /**
                 *
                 * @return
                 * The assigneesUrl
                 */
                public String getAssigneesUrl() {
                    return assigneesUrl;
                }

                /**
                 *
                 * @param assigneesUrl
                 * The assignees_url
                 */
                public void setAssigneesUrl(String assigneesUrl) {
                    this.assigneesUrl = assigneesUrl;
                }

                /**
                 *
                 * @return
                 * The blobsUrl
                 */
                public String getBlobsUrl() {
                    return blobsUrl;
                }

                /**
                 *
                 * @param blobsUrl
                 * The blobs_url
                 */
                public void setBlobsUrl(String blobsUrl) {
                    this.blobsUrl = blobsUrl;
                }

                /**
                 *
                 * @return
                 * The branchesUrl
                 */
                public String getBranchesUrl() {
                    return branchesUrl;
                }

                /**
                 *
                 * @param branchesUrl
                 * The branches_url
                 */
                public void setBranchesUrl(String branchesUrl) {
                    this.branchesUrl = branchesUrl;
                }

                /**
                 *
                 * @return
                 * The cloneUrl
                 */
                public String getCloneUrl() {
                    return cloneUrl;
                }

                /**
                 *
                 * @param cloneUrl
                 * The clone_url
                 */
                public void setCloneUrl(String cloneUrl) {
                    this.cloneUrl = cloneUrl;
                }

                /**
                 *
                 * @return
                 * The collaboratorsUrl
                 */
                public String getCollaboratorsUrl() {
                    return collaboratorsUrl;
                }

                /**
                 *
                 * @param collaboratorsUrl
                 * The collaborators_url
                 */
                public void setCollaboratorsUrl(String collaboratorsUrl) {
                    this.collaboratorsUrl = collaboratorsUrl;
                }

                /**
                 *
                 * @return
                 * The commentsUrl
                 */
                public String getCommentsUrl() {
                    return commentsUrl;
                }

                /**
                 *
                 * @param commentsUrl
                 * The comments_url
                 */
                public void setCommentsUrl(String commentsUrl) {
                    this.commentsUrl = commentsUrl;
                }

                /**
                 *
                 * @return
                 * The commitsUrl
                 */
                public String getCommitsUrl() {
                    return commitsUrl;
                }

                /**
                 *
                 * @param commitsUrl
                 * The commits_url
                 */
                public void setCommitsUrl(String commitsUrl) {
                    this.commitsUrl = commitsUrl;
                }

                /**
                 *
                 * @return
                 * The compareUrl
                 */
                public String getCompareUrl() {
                    return compareUrl;
                }

                /**
                 *
                 * @param compareUrl
                 * The compare_url
                 */
                public void setCompareUrl(String compareUrl) {
                    this.compareUrl = compareUrl;
                }

                /**
                 *
                 * @return
                 * The contentsUrl
                 */
                public String getContentsUrl() {
                    return contentsUrl;
                }

                /**
                 *
                 * @param contentsUrl
                 * The contents_url
                 */
                public void setContentsUrl(String contentsUrl) {
                    this.contentsUrl = contentsUrl;
                }

                /**
                 *
                 * @return
                 * The contributorsUrl
                 */
                public String getContributorsUrl() {
                    return contributorsUrl;
                }

                /**
                 *
                 * @param contributorsUrl
                 * The contributors_url
                 */
                public void setContributorsUrl(String contributorsUrl) {
                    this.contributorsUrl = contributorsUrl;
                }

                /**
                 *
                 * @return
                 * The downloadsUrl
                 */
                public String getDownloadsUrl() {
                    return downloadsUrl;
                }

                /**
                 *
                 * @param downloadsUrl
                 * The downloads_url
                 */
                public void setDownloadsUrl(String downloadsUrl) {
                    this.downloadsUrl = downloadsUrl;
                }

                /**
                 *
                 * @return
                 * The eventsUrl
                 */
                public String getEventsUrl() {
                    return eventsUrl;
                }

                /**
                 *
                 * @param eventsUrl
                 * The events_url
                 */
                public void setEventsUrl(String eventsUrl) {
                    this.eventsUrl = eventsUrl;
                }

                /**
                 *
                 * @return
                 * The forksUrl
                 */
                public String getForksUrl() {
                    return forksUrl;
                }

                /**
                 *
                 * @param forksUrl
                 * The forks_url
                 */
                public void setForksUrl(String forksUrl) {
                    this.forksUrl = forksUrl;
                }

                /**
                 *
                 * @return
                 * The gitCommitsUrl
                 */
                public String getGitCommitsUrl() {
                    return gitCommitsUrl;
                }

                /**
                 *
                 * @param gitCommitsUrl
                 * The git_commits_url
                 */
                public void setGitCommitsUrl(String gitCommitsUrl) {
                    this.gitCommitsUrl = gitCommitsUrl;
                }

                /**
                 *
                 * @return
                 * The gitRefsUrl
                 */
                public String getGitRefsUrl() {
                    return gitRefsUrl;
                }

                /**
                 *
                 * @param gitRefsUrl
                 * The git_refs_url
                 */
                public void setGitRefsUrl(String gitRefsUrl) {
                    this.gitRefsUrl = gitRefsUrl;
                }

                /**
                 *
                 * @return
                 * The gitTagsUrl
                 */
                public String getGitTagsUrl() {
                    return gitTagsUrl;
                }

                /**
                 *
                 * @param gitTagsUrl
                 * The git_tags_url
                 */
                public void setGitTagsUrl(String gitTagsUrl) {
                    this.gitTagsUrl = gitTagsUrl;
                }

                /**
                 *
                 * @return
                 * The gitUrl
                 */
                public String getGitUrl() {
                    return gitUrl;
                }

                /**
                 *
                 * @param gitUrl
                 * The git_url
                 */
                public void setGitUrl(String gitUrl) {
                    this.gitUrl = gitUrl;
                }

                /**
                 *
                 * @return
                 * The hooksUrl
                 */
                public String getHooksUrl() {
                    return hooksUrl;
                }

                /**
                 *
                 * @param hooksUrl
                 * The hooks_url
                 */
                public void setHooksUrl(String hooksUrl) {
                    this.hooksUrl = hooksUrl;
                }

                /**
                 *
                 * @return
                 * The issueCommentUrl
                 */
                public String getIssueCommentUrl() {
                    return issueCommentUrl;
                }

                /**
                 *
                 * @param issueCommentUrl
                 * The issue_comment_url
                 */
                public void setIssueCommentUrl(String issueCommentUrl) {
                    this.issueCommentUrl = issueCommentUrl;
                }

                /**
                 *
                 * @return
                 * The issueEventsUrl
                 */
                public String getIssueEventsUrl() {
                    return issueEventsUrl;
                }

                /**
                 *
                 * @param issueEventsUrl
                 * The issue_events_url
                 */
                public void setIssueEventsUrl(String issueEventsUrl) {
                    this.issueEventsUrl = issueEventsUrl;
                }

                /**
                 *
                 * @return
                 * The issuesUrl
                 */
                public String getIssuesUrl() {
                    return issuesUrl;
                }

                /**
                 *
                 * @param issuesUrl
                 * The issues_url
                 */
                public void setIssuesUrl(String issuesUrl) {
                    this.issuesUrl = issuesUrl;
                }

                /**
                 *
                 * @return
                 * The keysUrl
                 */
                public String getKeysUrl() {
                    return keysUrl;
                }

                /**
                 *
                 * @param keysUrl
                 * The keys_url
                 */
                public void setKeysUrl(String keysUrl) {
                    this.keysUrl = keysUrl;
                }

                /**
                 *
                 * @return
                 * The labelsUrl
                 */
                public String getLabelsUrl() {
                    return labelsUrl;
                }

                /**
                 *
                 * @param labelsUrl
                 * The labels_url
                 */
                public void setLabelsUrl(String labelsUrl) {
                    this.labelsUrl = labelsUrl;
                }

                /**
                 *
                 * @return
                 * The languagesUrl
                 */
                public String getLanguagesUrl() {
                    return languagesUrl;
                }

                /**
                 *
                 * @param languagesUrl
                 * The languages_url
                 */
                public void setLanguagesUrl(String languagesUrl) {
                    this.languagesUrl = languagesUrl;
                }

                /**
                 *
                 * @return
                 * The mergesUrl
                 */
                public String getMergesUrl() {
                    return mergesUrl;
                }

                /**
                 *
                 * @param mergesUrl
                 * The merges_url
                 */
                public void setMergesUrl(String mergesUrl) {
                    this.mergesUrl = mergesUrl;
                }

                /**
                 *
                 * @return
                 * The milestonesUrl
                 */
                public String getMilestonesUrl() {
                    return milestonesUrl;
                }

                /**
                 *
                 * @param milestonesUrl
                 * The milestones_url
                 */
                public void setMilestonesUrl(String milestonesUrl) {
                    this.milestonesUrl = milestonesUrl;
                }

                /**
                 *
                 * @return
                 * The mirrorUrl
                 */
                public String getMirrorUrl() {
                    return mirrorUrl;
                }

                /**
                 *
                 * @param mirrorUrl
                 * The mirror_url
                 */
                public void setMirrorUrl(String mirrorUrl) {
                    this.mirrorUrl = mirrorUrl;
                }

                /**
                 *
                 * @return
                 * The notificationsUrl
                 */
                public String getNotificationsUrl() {
                    return notificationsUrl;
                }

                /**
                 *
                 * @param notificationsUrl
                 * The notifications_url
                 */
                public void setNotificationsUrl(String notificationsUrl) {
                    this.notificationsUrl = notificationsUrl;
                }

                /**
                 *
                 * @return
                 * The pullsUrl
                 */
                public String getPullsUrl() {
                    return pullsUrl;
                }

                /**
                 *
                 * @param pullsUrl
                 * The pulls_url
                 */
                public void setPullsUrl(String pullsUrl) {
                    this.pullsUrl = pullsUrl;
                }

                /**
                 *
                 * @return
                 * The releasesUrl
                 */
                public String getReleasesUrl() {
                    return releasesUrl;
                }

                /**
                 *
                 * @param releasesUrl
                 * The releases_url
                 */
                public void setReleasesUrl(String releasesUrl) {
                    this.releasesUrl = releasesUrl;
                }

                /**
                 *
                 * @return
                 * The sshUrl
                 */
                public String getSshUrl() {
                    return sshUrl;
                }

                /**
                 *
                 * @param sshUrl
                 * The ssh_url
                 */
                public void setSshUrl(String sshUrl) {
                    this.sshUrl = sshUrl;
                }

                /**
                 *
                 * @return
                 * The stargazersUrl
                 */
                public String getStargazersUrl() {
                    return stargazersUrl;
                }

                /**
                 *
                 * @param stargazersUrl
                 * The stargazers_url
                 */
                public void setStargazersUrl(String stargazersUrl) {
                    this.stargazersUrl = stargazersUrl;
                }

                /**
                 *
                 * @return
                 * The statusesUrl
                 */
                public String getStatusesUrl() {
                    return statusesUrl;
                }

                /**
                 *
                 * @param statusesUrl
                 * The statuses_url
                 */
                public void setStatusesUrl(String statusesUrl) {
                    this.statusesUrl = statusesUrl;
                }

                /**
                 *
                 * @return
                 * The subscribersUrl
                 */
                public String getSubscribersUrl() {
                    return subscribersUrl;
                }

                /**
                 *
                 * @param subscribersUrl
                 * The subscribers_url
                 */
                public void setSubscribersUrl(String subscribersUrl) {
                    this.subscribersUrl = subscribersUrl;
                }

                /**
                 *
                 * @return
                 * The subscriptionUrl
                 */
                public String getSubscriptionUrl() {
                    return subscriptionUrl;
                }

                /**
                 *
                 * @param subscriptionUrl
                 * The subscription_url
                 */
                public void setSubscriptionUrl(String subscriptionUrl) {
                    this.subscriptionUrl = subscriptionUrl;
                }

                /**
                 *
                 * @return
                 * The svnUrl
                 */
                public String getSvnUrl() {
                    return svnUrl;
                }

                /**
                 *
                 * @param svnUrl
                 * The svn_url
                 */
                public void setSvnUrl(String svnUrl) {
                    this.svnUrl = svnUrl;
                }

                /**
                 *
                 * @return
                 * The tagsUrl
                 */
                public String getTagsUrl() {
                    return tagsUrl;
                }

                /**
                 *
                 * @param tagsUrl
                 * The tags_url
                 */
                public void setTagsUrl(String tagsUrl) {
                    this.tagsUrl = tagsUrl;
                }

                /**
                 *
                 * @return
                 * The teamsUrl
                 */
                public String getTeamsUrl() {
                    return teamsUrl;
                }

                /**
                 *
                 * @param teamsUrl
                 * The teams_url
                 */
                public void setTeamsUrl(String teamsUrl) {
                    this.teamsUrl = teamsUrl;
                }

                /**
                 *
                 * @return
                 * The treesUrl
                 */
                public String getTreesUrl() {
                    return treesUrl;
                }

                /**
                 *
                 * @param treesUrl
                 * The trees_url
                 */
                public void setTreesUrl(String treesUrl) {
                    this.treesUrl = treesUrl;
                }

                /**
                 *
                 * @return
                 * The homepage
                 */
                public String getHomepage() {
                    return homepage;
                }

                /**
                 *
                 * @param homepage
                 * The homepage
                 */
                public void setHomepage(String homepage) {
                    this.homepage = homepage;
                }

                /**
                 *
                 * @return
                 * The language
                 */
                public Object getLanguage() {
                    return language;
                }

                /**
                 *
                 * @param language
                 * The language
                 */
                public void setLanguage(Object language) {
                    this.language = language;
                }

                /**
                 *
                 * @return
                 * The forksCount
                 */
                public Integer getForksCount() {
                    return forksCount;
                }

                /**
                 *
                 * @param forksCount
                 * The forks_count
                 */
                public void setForksCount(Integer forksCount) {
                    this.forksCount = forksCount;
                }

                /**
                 *
                 * @return
                 * The stargazersCount
                 */
                public Integer getStargazersCount() {
                    return stargazersCount;
                }

                /**
                 *
                 * @param stargazersCount
                 * The stargazers_count
                 */
                public void setStargazersCount(Integer stargazersCount) {
                    this.stargazersCount = stargazersCount;
                }

                /**
                 *
                 * @return
                 * The watchersCount
                 */
                public Integer getWatchersCount() {
                    return watchersCount;
                }

                /**
                 *
                 * @param watchersCount
                 * The watchers_count
                 */
                public void setWatchersCount(Integer watchersCount) {
                    this.watchersCount = watchersCount;
                }

                /**
                 *
                 * @return
                 * The size
                 */
                public Integer getSize() {
                    return size;
                }

                /**
                 *
                 * @param size
                 * The size
                 */
                public void setSize(Integer size) {
                    this.size = size;
                }

                /**
                 *
                 * @return
                 * The defaultBranch
                 */
                public String getDefaultBranch() {
                    return defaultBranch;
                }

                /**
                 *
                 * @param defaultBranch
                 * The default_branch
                 */
                public void setDefaultBranch(String defaultBranch) {
                    this.defaultBranch = defaultBranch;
                }

                /**
                 *
                 * @return
                 * The openIssuesCount
                 */
                public Integer getOpenIssuesCount() {
                    return openIssuesCount;
                }

                /**
                 *
                 * @param openIssuesCount
                 * The open_issues_count
                 */
                public void setOpenIssuesCount(Integer openIssuesCount) {
                    this.openIssuesCount = openIssuesCount;
                }

                /**
                 *
                 * @return
                 * The hasIssues
                 */
                public Boolean getHasIssues() {
                    return hasIssues;
                }

                /**
                 *
                 * @param hasIssues
                 * The has_issues
                 */
                public void setHasIssues(Boolean hasIssues) {
                    this.hasIssues = hasIssues;
                }

                /**
                 *
                 * @return
                 * The hasWiki
                 */
                public Boolean getHasWiki() {
                    return hasWiki;
                }

                /**
                 *
                 * @param hasWiki
                 * The has_wiki
                 */
                public void setHasWiki(Boolean hasWiki) {
                    this.hasWiki = hasWiki;
                }

                /**
                 *
                 * @return
                 * The hasPages
                 */
                public Boolean getHasPages() {
                    return hasPages;
                }

                /**
                 *
                 * @param hasPages
                 * The has_pages
                 */
                public void setHasPages(Boolean hasPages) {
                    this.hasPages = hasPages;
                }

                /**
                 *
                 * @return
                 * The hasDownloads
                 */
                public Boolean getHasDownloads() {
                    return hasDownloads;
                }

                /**
                 *
                 * @param hasDownloads
                 * The has_downloads
                 */
                public void setHasDownloads(Boolean hasDownloads) {
                    this.hasDownloads = hasDownloads;
                }

                /**
                 *
                 * @return
                 * The pushedAt
                 */
                public String getPushedAt() {
                    return pushedAt;
                }

                /**
                 *
                 * @param pushedAt
                 * The pushed_at
                 */
                public void setPushedAt(String pushedAt) {
                    this.pushedAt = pushedAt;
                }

                /**
                 *
                 * @return
                 * The createdAt
                 */
                public String getCreatedAt() {
                    return createdAt;
                }

                /**
                 *
                 * @param createdAt
                 * The created_at
                 */
                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                /**
                 *
                 * @return
                 * The updatedAt
                 */
                public String getUpdatedAt() {
                    return updatedAt;
                }

                /**
                 *
                 * @param updatedAt
                 * The updated_at
                 */
                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }

                /**
                 *
                 * @return
                 * The permissions
                 */
                public Permissions_ getPermissions() {
                    return permissions;
                }

                /**
                 *
                 * @param permissions
                 * The permissions
                 */
                public void setPermissions(Permissions_ permissions) {
                    this.permissions = permissions;
                }

                public class Owner_ implements Parcelable {

                    @SerializedName("login")
                    @Expose
                    private String login;
                    @SerializedName("id")
                    @Expose
                    private Integer id;
                    @SerializedName("avatar_url")
                    @Expose
                    private String avatarUrl;
                    @SerializedName("gravatar_id")
                    @Expose
                    private String gravatarId;
                    @SerializedName("url")
                    @Expose
                    private String url;
                    @SerializedName("html_url")
                    @Expose
                    private String htmlUrl;
                    @SerializedName("followers_url")
                    @Expose
                    private String followersUrl;
                    @SerializedName("following_url")
                    @Expose
                    private String followingUrl;
                    @SerializedName("gists_url")
                    @Expose
                    private String gistsUrl;
                    @SerializedName("starred_url")
                    @Expose
                    private String starredUrl;
                    @SerializedName("subscriptions_url")
                    @Expose
                    private String subscriptionsUrl;
                    @SerializedName("organizations_url")
                    @Expose
                    private String organizationsUrl;
                    @SerializedName("repos_url")
                    @Expose
                    private String reposUrl;
                    @SerializedName("events_url")
                    @Expose
                    private String eventsUrl;
                    @SerializedName("received_events_url")
                    @Expose
                    private String receivedEventsUrl;
                    @SerializedName("type")
                    @Expose
                    private String type;
                    @SerializedName("site_admin")
                    @Expose
                    private Boolean siteAdmin;

                    /**
                     *
                     * @return
                     * The login
                     */
                    public String getLogin() {
                        return login;
                    }

                    /**
                     *
                     * @param login
                     * The login
                     */
                    public void setLogin(String login) {
                        this.login = login;
                    }

                    /**
                     *
                     * @return
                     * The id
                     */
                    public Integer getId() {
                        return id;
                    }

                    /**
                     *
                     * @param id
                     * The id
                     */
                    public void setId(Integer id) {
                        this.id = id;
                    }

                    /**
                     *
                     * @return
                     * The avatarUrl
                     */
                    public String getAvatarUrl() {
                        return avatarUrl;
                    }

                    /**
                     *
                     * @param avatarUrl
                     * The avatar_url
                     */
                    public void setAvatarUrl(String avatarUrl) {
                        this.avatarUrl = avatarUrl;
                    }

                    /**
                     *
                     * @return
                     * The gravatarId
                     */
                    public String getGravatarId() {
                        return gravatarId;
                    }

                    /**
                     *
                     * @param gravatarId
                     * The gravatar_id
                     */
                    public void setGravatarId(String gravatarId) {
                        this.gravatarId = gravatarId;
                    }

                    /**
                     *
                     * @return
                     * The url
                     */
                    public String getUrl() {
                        return url;
                    }

                    /**
                     *
                     * @param url
                     * The url
                     */
                    public void setUrl(String url) {
                        this.url = url;
                    }

                    /**
                     *
                     * @return
                     * The htmlUrl
                     */
                    public String getHtmlUrl() {
                        return htmlUrl;
                    }

                    /**
                     *
                     * @param htmlUrl
                     * The html_url
                     */
                    public void setHtmlUrl(String htmlUrl) {
                        this.htmlUrl = htmlUrl;
                    }

                    /**
                     *
                     * @return
                     * The followersUrl
                     */
                    public String getFollowersUrl() {
                        return followersUrl;
                    }

                    /**
                     *
                     * @param followersUrl
                     * The followers_url
                     */
                    public void setFollowersUrl(String followersUrl) {
                        this.followersUrl = followersUrl;
                    }

                    /**
                     *
                     * @return
                     * The followingUrl
                     */
                    public String getFollowingUrl() {
                        return followingUrl;
                    }

                    /**
                     *
                     * @param followingUrl
                     * The following_url
                     */
                    public void setFollowingUrl(String followingUrl) {
                        this.followingUrl = followingUrl;
                    }

                    /**
                     *
                     * @return
                     * The gistsUrl
                     */
                    public String getGistsUrl() {
                        return gistsUrl;
                    }

                    /**
                     *
                     * @param gistsUrl
                     * The gists_url
                     */
                    public void setGistsUrl(String gistsUrl) {
                        this.gistsUrl = gistsUrl;
                    }

                    /**
                     *
                     * @return
                     * The starredUrl
                     */
                    public String getStarredUrl() {
                        return starredUrl;
                    }

                    /**
                     *
                     * @param starredUrl
                     * The starred_url
                     */
                    public void setStarredUrl(String starredUrl) {
                        this.starredUrl = starredUrl;
                    }

                    /**
                     *
                     * @return
                     * The subscriptionsUrl
                     */
                    public String getSubscriptionsUrl() {
                        return subscriptionsUrl;
                    }

                    /**
                     *
                     * @param subscriptionsUrl
                     * The subscriptions_url
                     */
                    public void setSubscriptionsUrl(String subscriptionsUrl) {
                        this.subscriptionsUrl = subscriptionsUrl;
                    }

                    /**
                     *
                     * @return
                     * The organizationsUrl
                     */
                    public String getOrganizationsUrl() {
                        return organizationsUrl;
                    }

                    /**
                     *
                     * @param organizationsUrl
                     * The organizations_url
                     */
                    public void setOrganizationsUrl(String organizationsUrl) {
                        this.organizationsUrl = organizationsUrl;
                    }

                    /**
                     *
                     * @return
                     * The reposUrl
                     */
                    public String getReposUrl() {
                        return reposUrl;
                    }

                    /**
                     *
                     * @param reposUrl
                     * The repos_url
                     */
                    public void setReposUrl(String reposUrl) {
                        this.reposUrl = reposUrl;
                    }

                    /**
                     *
                     * @return
                     * The eventsUrl
                     */
                    public String getEventsUrl() {
                        return eventsUrl;
                    }

                    /**
                     *
                     * @param eventsUrl
                     * The events_url
                     */
                    public void setEventsUrl(String eventsUrl) {
                        this.eventsUrl = eventsUrl;
                    }

                    /**
                     *
                     * @return
                     * The receivedEventsUrl
                     */
                    public String getReceivedEventsUrl() {
                        return receivedEventsUrl;
                    }

                    /**
                     *
                     * @param receivedEventsUrl
                     * The received_events_url
                     */
                    public void setReceivedEventsUrl(String receivedEventsUrl) {
                        this.receivedEventsUrl = receivedEventsUrl;
                    }

                    /**
                     *
                     * @return
                     * The type
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     *
                     * @param type
                     * The type
                     */
                    public void setType(String type) {
                        this.type = type;
                    }

                    /**
                     *
                     * @return
                     * The siteAdmin
                     */
                    public Boolean getSiteAdmin() {
                        return siteAdmin;
                    }

                    /**
                     *
                     * @param siteAdmin
                     * The site_admin
                     */
                    public void setSiteAdmin(Boolean siteAdmin) {
                        this.siteAdmin = siteAdmin;
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeString(this.login);
                        dest.writeValue(this.id);
                        dest.writeString(this.avatarUrl);
                        dest.writeString(this.gravatarId);
                        dest.writeString(this.url);
                        dest.writeString(this.htmlUrl);
                        dest.writeString(this.followersUrl);
                        dest.writeString(this.followingUrl);
                        dest.writeString(this.gistsUrl);
                        dest.writeString(this.starredUrl);
                        dest.writeString(this.subscriptionsUrl);
                        dest.writeString(this.organizationsUrl);
                        dest.writeString(this.reposUrl);
                        dest.writeString(this.eventsUrl);
                        dest.writeString(this.receivedEventsUrl);
                        dest.writeString(this.type);
                        dest.writeValue(this.siteAdmin);
                    }

                    public Owner_() {
                    }

                    protected Owner_(Parcel in) {
                        this.login = in.readString();
                        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
                        this.avatarUrl = in.readString();
                        this.gravatarId = in.readString();
                        this.url = in.readString();
                        this.htmlUrl = in.readString();
                        this.followersUrl = in.readString();
                        this.followingUrl = in.readString();
                        this.gistsUrl = in.readString();
                        this.starredUrl = in.readString();
                        this.subscriptionsUrl = in.readString();
                        this.organizationsUrl = in.readString();
                        this.reposUrl = in.readString();
                        this.eventsUrl = in.readString();
                        this.receivedEventsUrl = in.readString();
                        this.type = in.readString();
                        this.siteAdmin = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    }

                    public final Creator<Owner_> CREATOR = new Creator<Owner_>() {
                        public Owner_ createFromParcel(Parcel source) {
                            return new Owner_(source);
                        }

                        public Owner_[] newArray(int size) {
                            return new Owner_[size];
                        }
                    };
                }

                public class Permissions_ implements Parcelable {

                    @SerializedName("admin")
                    @Expose
                    private Boolean admin;
                    @SerializedName("push")
                    @Expose
                    private Boolean push;
                    @SerializedName("pull")
                    @Expose
                    private Boolean pull;

                    /**
                     *
                     * @return
                     * The admin
                     */
                    public Boolean getAdmin() {
                        return admin;
                    }

                    /**
                     *
                     * @param admin
                     * The admin
                     */
                    public void setAdmin(Boolean admin) {
                        this.admin = admin;
                    }

                    /**
                     *
                     * @return
                     * The push
                     */
                    public Boolean getPush() {
                        return push;
                    }

                    /**
                     *
                     * @param push
                     * The push
                     */
                    public void setPush(Boolean push) {
                        this.push = push;
                    }

                    /**
                     *
                     * @return
                     * The pull
                     */
                    public Boolean getPull() {
                        return pull;
                    }

                    /**
                     *
                     * @param pull
                     * The pull
                     */
                    public void setPull(Boolean pull) {
                        this.pull = pull;
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeValue(this.admin);
                        dest.writeValue(this.push);
                        dest.writeValue(this.pull);
                    }

                    public Permissions_() {
                    }

                    protected Permissions_(Parcel in) {
                        this.admin = (Boolean) in.readValue(Boolean.class.getClassLoader());
                        this.push = (Boolean) in.readValue(Boolean.class.getClassLoader());
                        this.pull = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    }

                    public final Creator<Permissions_> CREATOR = new Creator<Permissions_>() {
                        public Permissions_ createFromParcel(Parcel source) {
                            return new Permissions_(source);
                        }

                        public Permissions_[] newArray(int size) {
                            return new Permissions_[size];
                        }
                    };
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeValue(this.id);
                    dest.writeParcelable(this.owner, flags);
                    dest.writeString(this.name);
                    dest.writeString(this.fullName);
                    dest.writeString(this.description);
                    dest.writeValue(this._private);
                    dest.writeValue(this.fork);
                    dest.writeString(this.url);
                    dest.writeString(this.htmlUrl);
                    dest.writeString(this.archiveUrl);
                    dest.writeString(this.assigneesUrl);
                    dest.writeString(this.blobsUrl);
                    dest.writeString(this.branchesUrl);
                    dest.writeString(this.cloneUrl);
                    dest.writeString(this.collaboratorsUrl);
                    dest.writeString(this.commentsUrl);
                    dest.writeString(this.commitsUrl);
                    dest.writeString(this.compareUrl);
                    dest.writeString(this.contentsUrl);
                    dest.writeString(this.contributorsUrl);
                    dest.writeString(this.downloadsUrl);
                    dest.writeString(this.eventsUrl);
                    dest.writeString(this.forksUrl);
                    dest.writeString(this.gitCommitsUrl);
                    dest.writeString(this.gitRefsUrl);
                    dest.writeString(this.gitTagsUrl);
                    dest.writeString(this.gitUrl);
                    dest.writeString(this.hooksUrl);
                    dest.writeString(this.issueCommentUrl);
                    dest.writeString(this.issueEventsUrl);
                    dest.writeString(this.issuesUrl);
                    dest.writeString(this.keysUrl);
                    dest.writeString(this.labelsUrl);
                    dest.writeString(this.languagesUrl);
                    dest.writeString(this.mergesUrl);
                    dest.writeString(this.milestonesUrl);
                    dest.writeString(this.mirrorUrl);
                    dest.writeString(this.notificationsUrl);
                    dest.writeString(this.pullsUrl);
                    dest.writeString(this.releasesUrl);
                    dest.writeString(this.sshUrl);
                    dest.writeString(this.stargazersUrl);
                    dest.writeString(this.statusesUrl);
                    dest.writeString(this.subscribersUrl);
                    dest.writeString(this.subscriptionUrl);
                    dest.writeString(this.svnUrl);
                    dest.writeString(this.tagsUrl);
                    dest.writeString(this.teamsUrl);
                    dest.writeString(this.treesUrl);
                    dest.writeString(this.homepage);
                    dest.writeValue(this.language);
                    dest.writeValue(this.forksCount);
                    dest.writeValue(this.stargazersCount);
                    dest.writeValue(this.watchersCount);
                    dest.writeValue(this.size);
                    dest.writeString(this.defaultBranch);
                    dest.writeValue(this.openIssuesCount);
                    dest.writeValue(this.hasIssues);
                    dest.writeValue(this.hasWiki);
                    dest.writeValue(this.hasPages);
                    dest.writeValue(this.hasDownloads);
                    dest.writeString(this.pushedAt);
                    dest.writeString(this.createdAt);
                    dest.writeString(this.updatedAt);
                    dest.writeParcelable(this.permissions, flags);
                }

                public Repo_() {
                }

                protected Repo_(Parcel in) {
                    this.id = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.owner = in.readParcelable(Owner_.class.getClassLoader());
                    this.name = in.readString();
                    this.fullName = in.readString();
                    this.description = in.readString();
                    this._private = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.fork = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.url = in.readString();
                    this.htmlUrl = in.readString();
                    this.archiveUrl = in.readString();
                    this.assigneesUrl = in.readString();
                    this.blobsUrl = in.readString();
                    this.branchesUrl = in.readString();
                    this.cloneUrl = in.readString();
                    this.collaboratorsUrl = in.readString();
                    this.commentsUrl = in.readString();
                    this.commitsUrl = in.readString();
                    this.compareUrl = in.readString();
                    this.contentsUrl = in.readString();
                    this.contributorsUrl = in.readString();
                    this.downloadsUrl = in.readString();
                    this.eventsUrl = in.readString();
                    this.forksUrl = in.readString();
                    this.gitCommitsUrl = in.readString();
                    this.gitRefsUrl = in.readString();
                    this.gitTagsUrl = in.readString();
                    this.gitUrl = in.readString();
                    this.hooksUrl = in.readString();
                    this.issueCommentUrl = in.readString();
                    this.issueEventsUrl = in.readString();
                    this.issuesUrl = in.readString();
                    this.keysUrl = in.readString();
                    this.labelsUrl = in.readString();
                    this.languagesUrl = in.readString();
                    this.mergesUrl = in.readString();
                    this.milestonesUrl = in.readString();
                    this.mirrorUrl = in.readString();
                    this.notificationsUrl = in.readString();
                    this.pullsUrl = in.readString();
                    this.releasesUrl = in.readString();
                    this.sshUrl = in.readString();
                    this.stargazersUrl = in.readString();
                    this.statusesUrl = in.readString();
                    this.subscribersUrl = in.readString();
                    this.subscriptionUrl = in.readString();
                    this.svnUrl = in.readString();
                    this.tagsUrl = in.readString();
                    this.teamsUrl = in.readString();
                    this.treesUrl = in.readString();
                    this.homepage = in.readString();
                    this.language = in.readParcelable(Object.class.getClassLoader());
                    this.forksCount = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.stargazersCount = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.watchersCount = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.size = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.defaultBranch = in.readString();
                    this.openIssuesCount = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.hasIssues = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.hasWiki = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.hasPages = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.hasDownloads = (Boolean) in.readValue(Boolean.class.getClassLoader());
                    this.pushedAt = in.readString();
                    this.createdAt = in.readString();
                    this.updatedAt = in.readString();
                    this.permissions = in.readParcelable(Permissions_.class.getClassLoader());
                }

                public final Creator<Repo_> CREATOR = new Creator<Repo_>() {
                    public Repo_ createFromParcel(Parcel source) {
                        return new Repo_(source);
                    }

                    public Repo_[] newArray(int size) {
                        return new Repo_[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.label);
                dest.writeString(this.ref);
                dest.writeString(this.sha);
                dest.writeParcelable(this.user, flags);
                dest.writeParcelable(this.repo, flags);
            }

            public Base() {
            }

            protected Base(Parcel in) {
                this.label = in.readString();
                this.ref = in.readString();
                this.sha = in.readString();
                this.user = in.readParcelable(User_.class.getClassLoader());
                this.repo = in.readParcelable(Repo_.class.getClassLoader());
            }

            public final Creator<Base> CREATOR = new Creator<Base>() {
                public Base createFromParcel(Parcel source) {
                    return new Base(source);
                }

                public Base[] newArray(int size) {
                    return new Base[size];
                }
            };
        }

        public class Assignee implements Parcelable {

            @SerializedName("login")
            @Expose
            private String login;
            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("avatar_url")
            @Expose
            private String avatarUrl;
            @SerializedName("gravatar_id")
            @Expose
            private String gravatarId;
            @SerializedName("url")
            @Expose
            private String url;
            @SerializedName("html_url")
            @Expose
            private String htmlUrl;
            @SerializedName("followers_url")
            @Expose
            private String followersUrl;
            @SerializedName("following_url")
            @Expose
            private String followingUrl;
            @SerializedName("gists_url")
            @Expose
            private String gistsUrl;
            @SerializedName("starred_url")
            @Expose
            private String starredUrl;
            @SerializedName("subscriptions_url")
            @Expose
            private String subscriptionsUrl;
            @SerializedName("organizations_url")
            @Expose
            private String organizationsUrl;
            @SerializedName("repos_url")
            @Expose
            private String reposUrl;
            @SerializedName("events_url")
            @Expose
            private String eventsUrl;
            @SerializedName("received_events_url")
            @Expose
            private String receivedEventsUrl;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("site_admin")
            @Expose
            private Boolean siteAdmin;

            /**
             *
             * @return
             * The login
             */
            public String getLogin() {
                return login;
            }

            /**
             *
             * @param login
             * The login
             */
            public void setLogin(String login) {
                this.login = login;
            }

            /**
             *
             * @return
             * The id
             */
            public Integer getId() {
                return id;
            }

            /**
             *
             * @param id
             * The id
             */
            public void setId(Integer id) {
                this.id = id;
            }

            /**
             *
             * @return
             * The avatarUrl
             */
            public String getAvatarUrl() {
                return avatarUrl;
            }

            /**
             *
             * @param avatarUrl
             * The avatar_url
             */
            public void setAvatarUrl(String avatarUrl) {
                this.avatarUrl = avatarUrl;
            }

            /**
             *
             * @return
             * The gravatarId
             */
            public String getGravatarId() {
                return gravatarId;
            }

            /**
             *
             * @param gravatarId
             * The gravatar_id
             */
            public void setGravatarId(String gravatarId) {
                this.gravatarId = gravatarId;
            }

            /**
             *
             * @return
             * The url
             */
            public String getUrl() {
                return url;
            }

            /**
             *
             * @param url
             * The url
             */
            public void setUrl(String url) {
                this.url = url;
            }

            /**
             *
             * @return
             * The htmlUrl
             */
            public String getHtmlUrl() {
                return htmlUrl;
            }

            /**
             *
             * @param htmlUrl
             * The html_url
             */
            public void setHtmlUrl(String htmlUrl) {
                this.htmlUrl = htmlUrl;
            }

            /**
             *
             * @return
             * The followersUrl
             */
            public String getFollowersUrl() {
                return followersUrl;
            }

            /**
             *
             * @param followersUrl
             * The followers_url
             */
            public void setFollowersUrl(String followersUrl) {
                this.followersUrl = followersUrl;
            }

            /**
             *
             * @return
             * The followingUrl
             */
            public String getFollowingUrl() {
                return followingUrl;
            }

            /**
             *
             * @param followingUrl
             * The following_url
             */
            public void setFollowingUrl(String followingUrl) {
                this.followingUrl = followingUrl;
            }

            /**
             *
             * @return
             * The gistsUrl
             */
            public String getGistsUrl() {
                return gistsUrl;
            }

            /**
             *
             * @param gistsUrl
             * The gists_url
             */
            public void setGistsUrl(String gistsUrl) {
                this.gistsUrl = gistsUrl;
            }

            /**
             *
             * @return
             * The starredUrl
             */
            public String getStarredUrl() {
                return starredUrl;
            }

            /**
             *
             * @param starredUrl
             * The starred_url
             */
            public void setStarredUrl(String starredUrl) {
                this.starredUrl = starredUrl;
            }

            /**
             *
             * @return
             * The subscriptionsUrl
             */
            public String getSubscriptionsUrl() {
                return subscriptionsUrl;
            }

            /**
             *
             * @param subscriptionsUrl
             * The subscriptions_url
             */
            public void setSubscriptionsUrl(String subscriptionsUrl) {
                this.subscriptionsUrl = subscriptionsUrl;
            }

            /**
             *
             * @return
             * The organizationsUrl
             */
            public String getOrganizationsUrl() {
                return organizationsUrl;
            }

            /**
             *
             * @param organizationsUrl
             * The organizations_url
             */
            public void setOrganizationsUrl(String organizationsUrl) {
                this.organizationsUrl = organizationsUrl;
            }

            /**
             *
             * @return
             * The reposUrl
             */
            public String getReposUrl() {
                return reposUrl;
            }

            /**
             *
             * @param reposUrl
             * The repos_url
             */
            public void setReposUrl(String reposUrl) {
                this.reposUrl = reposUrl;
            }

            /**
             *
             * @return
             * The eventsUrl
             */
            public String getEventsUrl() {
                return eventsUrl;
            }

            /**
             *
             * @param eventsUrl
             * The events_url
             */
            public void setEventsUrl(String eventsUrl) {
                this.eventsUrl = eventsUrl;
            }

            /**
             *
             * @return
             * The receivedEventsUrl
             */
            public String getReceivedEventsUrl() {
                return receivedEventsUrl;
            }

            /**
             *
             * @param receivedEventsUrl
             * The received_events_url
             */
            public void setReceivedEventsUrl(String receivedEventsUrl) {
                this.receivedEventsUrl = receivedEventsUrl;
            }

            /**
             *
             * @return
             * The type
             */
            public String getType() {
                return type;
            }

            /**
             *
             * @param type
             * The type
             */
            public void setType(String type) {
                this.type = type;
            }

            /**
             *
             * @return
             * The siteAdmin
             */
            public Boolean getSiteAdmin() {
                return siteAdmin;
            }

            /**
             *
             * @param siteAdmin
             * The site_admin
             */
            public void setSiteAdmin(Boolean siteAdmin) {
                this.siteAdmin = siteAdmin;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.login);
                dest.writeValue(this.id);
                dest.writeString(this.avatarUrl);
                dest.writeString(this.gravatarId);
                dest.writeString(this.url);
                dest.writeString(this.htmlUrl);
                dest.writeString(this.followersUrl);
                dest.writeString(this.followingUrl);
                dest.writeString(this.gistsUrl);
                dest.writeString(this.starredUrl);
                dest.writeString(this.subscriptionsUrl);
                dest.writeString(this.organizationsUrl);
                dest.writeString(this.reposUrl);
                dest.writeString(this.eventsUrl);
                dest.writeString(this.receivedEventsUrl);
                dest.writeString(this.type);
                dest.writeValue(this.siteAdmin);
            }

            public Assignee() {
            }

            protected Assignee(Parcel in) {
                this.login = in.readString();
                this.id = (Integer) in.readValue(Integer.class.getClassLoader());
                this.avatarUrl = in.readString();
                this.gravatarId = in.readString();
                this.url = in.readString();
                this.htmlUrl = in.readString();
                this.followersUrl = in.readString();
                this.followingUrl = in.readString();
                this.gistsUrl = in.readString();
                this.starredUrl = in.readString();
                this.subscriptionsUrl = in.readString();
                this.organizationsUrl = in.readString();
                this.reposUrl = in.readString();
                this.eventsUrl = in.readString();
                this.receivedEventsUrl = in.readString();
                this.type = in.readString();
                this.siteAdmin = (Boolean) in.readValue(Boolean.class.getClassLoader());
            }

            public final Parcelable.Creator<Assignee> CREATOR = new Parcelable.Creator<Assignee>() {
                public Assignee createFromParcel(Parcel source) {
                    return new Assignee(source);
                }

                public Assignee[] newArray(int size) {
                    return new Assignee[size];
                }
            };
        }

        public class Milestone implements Parcelable {

            @SerializedName("url")
            @Expose
            private String url;
            @SerializedName("html_url")
            @Expose
            private String htmlUrl;
            @SerializedName("labels_url")
            @Expose
            private String labelsUrl;
            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("number")
            @Expose
            private Integer number;
            @SerializedName("state")
            @Expose
            private String state;
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("description")
            @Expose
            private String description;
            @SerializedName("creator")
            @Expose
            private Creator creator;
            @SerializedName("open_issues")
            @Expose
            private Integer openIssues;
            @SerializedName("closed_issues")
            @Expose
            private Integer closedIssues;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;
            @SerializedName("closed_at")
            @Expose
            private String closedAt;
            @SerializedName("due_on")
            @Expose
            private String dueOn;

            /**
             *
             * @return
             * The url
             */
            public String getUrl() {
                return url;
            }

            /**
             *
             * @param url
             * The url
             */
            public void setUrl(String url) {
                this.url = url;
            }

            /**
             *
             * @return
             * The htmlUrl
             */
            public String getHtmlUrl() {
                return htmlUrl;
            }

            /**
             *
             * @param htmlUrl
             * The html_url
             */
            public void setHtmlUrl(String htmlUrl) {
                this.htmlUrl = htmlUrl;
            }

            /**
             *
             * @return
             * The labelsUrl
             */
            public String getLabelsUrl() {
                return labelsUrl;
            }

            /**
             *
             * @param labelsUrl
             * The labels_url
             */
            public void setLabelsUrl(String labelsUrl) {
                this.labelsUrl = labelsUrl;
            }

            /**
             *
             * @return
             * The id
             */
            public Integer getId() {
                return id;
            }

            /**
             *
             * @param id
             * The id
             */
            public void setId(Integer id) {
                this.id = id;
            }

            /**
             *
             * @return
             * The number
             */
            public Integer getNumber() {
                return number;
            }

            /**
             *
             * @param number
             * The number
             */
            public void setNumber(Integer number) {
                this.number = number;
            }

            /**
             *
             * @return
             * The state
             */
            public String getState() {
                return state;
            }

            /**
             *
             * @param state
             * The state
             */
            public void setState(String state) {
                this.state = state;
            }

            /**
             *
             * @return
             * The title
             */
            public String getTitle() {
                return title;
            }

            /**
             *
             * @param title
             * The title
             */
            public void setTitle(String title) {
                this.title = title;
            }

            /**
             *
             * @return
             * The description
             */
            public String getDescription() {
                return description;
            }

            /**
             *
             * @param description
             * The description
             */
            public void setDescription(String description) {
                this.description = description;
            }

            /**
             *
             * @return
             * The creator
             */
            public Creator getCreator() {
                return creator;
            }

            /**
             *
             * @param creator
             * The creator
             */
            public void setCreator(Creator creator) {
                this.creator = creator;
            }

            /**
             *
             * @return
             * The openIssues
             */
            public Integer getOpenIssues() {
                return openIssues;
            }

            /**
             *
             * @param openIssues
             * The open_issues
             */
            public void setOpenIssues(Integer openIssues) {
                this.openIssues = openIssues;
            }

            /**
             *
             * @return
             * The closedIssues
             */
            public Integer getClosedIssues() {
                return closedIssues;
            }

            /**
             *
             * @param closedIssues
             * The closed_issues
             */
            public void setClosedIssues(Integer closedIssues) {
                this.closedIssues = closedIssues;
            }

            /**
             *
             * @return
             * The createdAt
             */
            public String getCreatedAt() {
                return createdAt;
            }

            /**
             *
             * @param createdAt
             * The created_at
             */
            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            /**
             *
             * @return
             * The updatedAt
             */
            public String getUpdatedAt() {
                return updatedAt;
            }

            /**
             *
             * @param updatedAt
             * The updated_at
             */
            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            /**
             *
             * @return
             * The closedAt
             */
            public String getClosedAt() {
                return closedAt;
            }

            /**
             *
             * @param closedAt
             * The closed_at
             */
            public void setClosedAt(String closedAt) {
                this.closedAt = closedAt;
            }

            /**
             *
             * @return
             * The dueOn
             */
            public String getDueOn() {
                return dueOn;
            }

            /**
             *
             * @param dueOn
             * The due_on
             */
            public void setDueOn(String dueOn) {
                this.dueOn = dueOn;
            }

            public class Creator implements Parcelable {

                @SerializedName("login")
                @Expose
                private String login;
                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("avatar_url")
                @Expose
                private String avatarUrl;
                @SerializedName("gravatar_id")
                @Expose
                private String gravatarId;
                @SerializedName("url")
                @Expose
                private String url;
                @SerializedName("html_url")
                @Expose
                private String htmlUrl;
                @SerializedName("followers_url")
                @Expose
                private String followersUrl;
                @SerializedName("following_url")
                @Expose
                private String followingUrl;
                @SerializedName("gists_url")
                @Expose
                private String gistsUrl;
                @SerializedName("starred_url")
                @Expose
                private String starredUrl;
                @SerializedName("subscriptions_url")
                @Expose
                private String subscriptionsUrl;
                @SerializedName("organizations_url")
                @Expose
                private String organizationsUrl;
                @SerializedName("repos_url")
                @Expose
                private String reposUrl;
                @SerializedName("events_url")
                @Expose
                private String eventsUrl;
                @SerializedName("received_events_url")
                @Expose
                private String receivedEventsUrl;
                @SerializedName("type")
                @Expose
                private String type;
                @SerializedName("site_admin")
                @Expose
                private Boolean siteAdmin;

                /**
                 *
                 * @return
                 * The login
                 */
                public String getLogin() {
                    return login;
                }

                /**
                 *
                 * @param login
                 * The login
                 */
                public void setLogin(String login) {
                    this.login = login;
                }

                /**
                 *
                 * @return
                 * The id
                 */
                public Integer getId() {
                    return id;
                }

                /**
                 *
                 * @param id
                 * The id
                 */
                public void setId(Integer id) {
                    this.id = id;
                }

                /**
                 *
                 * @return
                 * The avatarUrl
                 */
                public String getAvatarUrl() {
                    return avatarUrl;
                }

                /**
                 *
                 * @param avatarUrl
                 * The avatar_url
                 */
                public void setAvatarUrl(String avatarUrl) {
                    this.avatarUrl = avatarUrl;
                }

                /**
                 *
                 * @return
                 * The gravatarId
                 */
                public String getGravatarId() {
                    return gravatarId;
                }

                /**
                 *
                 * @param gravatarId
                 * The gravatar_id
                 */
                public void setGravatarId(String gravatarId) {
                    this.gravatarId = gravatarId;
                }

                /**
                 *
                 * @return
                 * The url
                 */
                public String getUrl() {
                    return url;
                }

                /**
                 *
                 * @param url
                 * The url
                 */
                public void setUrl(String url) {
                    this.url = url;
                }

                /**
                 *
                 * @return
                 * The htmlUrl
                 */
                public String getHtmlUrl() {
                    return htmlUrl;
                }

                /**
                 *
                 * @param htmlUrl
                 * The html_url
                 */
                public void setHtmlUrl(String htmlUrl) {
                    this.htmlUrl = htmlUrl;
                }

                /**
                 *
                 * @return
                 * The followersUrl
                 */
                public String getFollowersUrl() {
                    return followersUrl;
                }

                /**
                 *
                 * @param followersUrl
                 * The followers_url
                 */
                public void setFollowersUrl(String followersUrl) {
                    this.followersUrl = followersUrl;
                }

                /**
                 *
                 * @return
                 * The followingUrl
                 */
                public String getFollowingUrl() {
                    return followingUrl;
                }

                /**
                 *
                 * @param followingUrl
                 * The following_url
                 */
                public void setFollowingUrl(String followingUrl) {
                    this.followingUrl = followingUrl;
                }

                /**
                 *
                 * @return
                 * The gistsUrl
                 */
                public String getGistsUrl() {
                    return gistsUrl;
                }

                /**
                 *
                 * @param gistsUrl
                 * The gists_url
                 */
                public void setGistsUrl(String gistsUrl) {
                    this.gistsUrl = gistsUrl;
                }

                /**
                 *
                 * @return
                 * The starredUrl
                 */
                public String getStarredUrl() {
                    return starredUrl;
                }

                /**
                 *
                 * @param starredUrl
                 * The starred_url
                 */
                public void setStarredUrl(String starredUrl) {
                    this.starredUrl = starredUrl;
                }

                /**
                 *
                 * @return
                 * The subscriptionsUrl
                 */
                public String getSubscriptionsUrl() {
                    return subscriptionsUrl;
                }

                /**
                 *
                 * @param subscriptionsUrl
                 * The subscriptions_url
                 */
                public void setSubscriptionsUrl(String subscriptionsUrl) {
                    this.subscriptionsUrl = subscriptionsUrl;
                }

                /**
                 *
                 * @return
                 * The organizationsUrl
                 */
                public String getOrganizationsUrl() {
                    return organizationsUrl;
                }

                /**
                 *
                 * @param organizationsUrl
                 * The organizations_url
                 */
                public void setOrganizationsUrl(String organizationsUrl) {
                    this.organizationsUrl = organizationsUrl;
                }

                /**
                 *
                 * @return
                 * The reposUrl
                 */
                public String getReposUrl() {
                    return reposUrl;
                }

                /**
                 *
                 * @param reposUrl
                 * The repos_url
                 */
                public void setReposUrl(String reposUrl) {
                    this.reposUrl = reposUrl;
                }

                /**
                 *
                 * @return
                 * The eventsUrl
                 */
                public String getEventsUrl() {
                    return eventsUrl;
                }

                /**
                 *
                 * @param eventsUrl
                 * The events_url
                 */
                public void setEventsUrl(String eventsUrl) {
                    this.eventsUrl = eventsUrl;
                }

                /**
                 *
                 * @return
                 * The receivedEventsUrl
                 */
                public String getReceivedEventsUrl() {
                    return receivedEventsUrl;
                }

                /**
                 *
                 * @param receivedEventsUrl
                 * The received_events_url
                 */
                public void setReceivedEventsUrl(String receivedEventsUrl) {
                    this.receivedEventsUrl = receivedEventsUrl;
                }

                /**
                 *
                 * @return
                 * The type
                 */
                public String getType() {
                    return type;
                }

                /**
                 *
                 * @param type
                 * The type
                 */
                public void setType(String type) {
                    this.type = type;
                }

                /**
                 *
                 * @return
                 * The siteAdmin
                 */
                public Boolean getSiteAdmin() {
                    return siteAdmin;
                }

                /**
                 *
                 * @param siteAdmin
                 * The site_admin
                 */
                public void setSiteAdmin(Boolean siteAdmin) {
                    this.siteAdmin = siteAdmin;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.login);
                    dest.writeValue(this.id);
                    dest.writeString(this.avatarUrl);
                    dest.writeString(this.gravatarId);
                    dest.writeString(this.url);
                    dest.writeString(this.htmlUrl);
                    dest.writeString(this.followersUrl);
                    dest.writeString(this.followingUrl);
                    dest.writeString(this.gistsUrl);
                    dest.writeString(this.starredUrl);
                    dest.writeString(this.subscriptionsUrl);
                    dest.writeString(this.organizationsUrl);
                    dest.writeString(this.reposUrl);
                    dest.writeString(this.eventsUrl);
                    dest.writeString(this.receivedEventsUrl);
                    dest.writeString(this.type);
                    dest.writeValue(this.siteAdmin);
                }

                public Creator() {
                }

                protected Creator(Parcel in) {
                    this.login = in.readString();
                    this.id = (Integer) in.readValue(Integer.class.getClassLoader());
                    this.avatarUrl = in.readString();
                    this.gravatarId = in.readString();
                    this.url = in.readString();
                    this.htmlUrl = in.readString();
                    this.followersUrl = in.readString();
                    this.followingUrl = in.readString();
                    this.gistsUrl = in.readString();
                    this.starredUrl = in.readString();
                    this.subscriptionsUrl = in.readString();
                    this.organizationsUrl = in.readString();
                    this.reposUrl = in.readString();
                    this.eventsUrl = in.readString();
                    this.receivedEventsUrl = in.readString();
                    this.type = in.readString();
                    this.siteAdmin = (Boolean) in.readValue(Boolean.class.getClassLoader());
                }

                public final Creator<Milestone.Creator> CREATOR = new Creator<Milestone.Creator>() {
                    public Milestone.Creator createFromParcel(Parcel source) {
                        return new Milestone.Creator(source);
                    }

                    public Milestone.Creator[] newArray(int size) {
                        return new Milestone.Creator[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.url);
                dest.writeString(this.htmlUrl);
                dest.writeString(this.labelsUrl);
                dest.writeValue(this.id);
                dest.writeValue(this.number);
                dest.writeString(this.state);
                dest.writeString(this.title);
                dest.writeString(this.description);
                dest.writeParcelable(this.creator, flags);
                dest.writeValue(this.openIssues);
                dest.writeValue(this.closedIssues);
                dest.writeString(this.createdAt);
                dest.writeString(this.updatedAt);
                dest.writeString(this.closedAt);
                dest.writeString(this.dueOn);
            }

            public Milestone() {
            }

            protected Milestone(Parcel in) {
                this.url = in.readString();
                this.htmlUrl = in.readString();
                this.labelsUrl = in.readString();
                this.id = (Integer) in.readValue(Integer.class.getClassLoader());
                this.number = (Integer) in.readValue(Integer.class.getClassLoader());
                this.state = in.readString();
                this.title = in.readString();
                this.description = in.readString();
                this.creator = in.readParcelable(Creator.class.getClassLoader());
                this.openIssues = (Integer) in.readValue(Integer.class.getClassLoader());
                this.closedIssues = (Integer) in.readValue(Integer.class.getClassLoader());
                this.createdAt = in.readString();
                this.updatedAt = in.readString();
                this.closedAt = in.readString();
                this.dueOn = in.readString();
            }

            public final Parcelable.Creator<Milestone> CREATOR = new Parcelable.Creator<Milestone>() {
                public Milestone createFromParcel(Parcel source) {
                    return new Milestone(source);
                }

                public Milestone[] newArray(int size) {
                    return new Milestone[size];
                }
            };
        }

        public class Links implements Parcelable {

            @SerializedName("self")
            @Expose
            private Self self;
            @SerializedName("html")
            @Expose
            private Html html;
            @SerializedName("issue")
            @Expose
            private Issue issue;
            @SerializedName("comments")
            @Expose
            private Comments comments;
            @SerializedName("review_comments")
            @Expose
            private ReviewComments reviewComments;
            @SerializedName("review_comment")
            @Expose
            private ReviewComment reviewComment;
            @SerializedName("commits")
            @Expose
            private Commits commits;
            @SerializedName("statuses")
            @Expose
            private Statuses statuses;

            /**
             *
             * @return
             * The self
             */
            public Self getSelf() {
                return self;
            }

            /**
             *
             * @param self
             * The self
             */
            public void setSelf(Self self) {
                this.self = self;
            }

            /**
             *
             * @return
             * The html
             */
            public Html getHtml() {
                return html;
            }

            /**
             *
             * @param html
             * The html
             */
            public void setHtml(Html html) {
                this.html = html;
            }

            /**
             *
             * @return
             * The issue
             */
            public Issue getIssue() {
                return issue;
            }

            /**
             *
             * @param issue
             * The issue
             */
            public void setIssue(Issue issue) {
                this.issue = issue;
            }

            /**
             *
             * @return
             * The comments
             */
            public Comments getComments() {
                return comments;
            }

            /**
             *
             * @param comments
             * The comments
             */
            public void setComments(Comments comments) {
                this.comments = comments;
            }

            /**
             *
             * @return
             * The reviewComments
             */
            public ReviewComments getReviewComments() {
                return reviewComments;
            }

            /**
             *
             * @param reviewComments
             * The review_comments
             */
            public void setReviewComments(ReviewComments reviewComments) {
                this.reviewComments = reviewComments;
            }

            /**
             *
             * @return
             * The reviewComment
             */
            public ReviewComment getReviewComment() {
                return reviewComment;
            }

            /**
             *
             * @param reviewComment
             * The review_comment
             */
            public void setReviewComment(ReviewComment reviewComment) {
                this.reviewComment = reviewComment;
            }

            /**
             *
             * @return
             * The commits
             */
            public Commits getCommits() {
                return commits;
            }

            /**
             *
             * @param commits
             * The commits
             */
            public void setCommits(Commits commits) {
                this.commits = commits;
            }

            /**
             *
             * @return
             * The statuses
             */
            public Statuses getStatuses() {
                return statuses;
            }

            /**
             *
             * @param statuses
             * The statuses
             */
            public void setStatuses(Statuses statuses) {
                this.statuses = statuses;
            }

            public class Self implements Parcelable {

                @SerializedName("href")
                @Expose
                private String href;

                /**
                 *
                 * @return
                 * The href
                 */
                public String getHref() {
                    return href;
                }

                /**
                 *
                 * @param href
                 * The href
                 */
                public void setHref(String href) {
                    this.href = href;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.href);
                }

                public Self() {
                }

                protected Self(Parcel in) {
                    this.href = in.readString();
                }

                public final Creator<Self> CREATOR = new Creator<Self>() {
                    public Self createFromParcel(Parcel source) {
                        return new Self(source);
                    }

                    public Self[] newArray(int size) {
                        return new Self[size];
                    }
                };
            }

            public class Html implements Parcelable {

                @SerializedName("href")
                @Expose
                private String href;

                /**
                 *
                 * @return
                 * The href
                 */
                public String getHref() {
                    return href;
                }

                /**
                 *
                 * @param href
                 * The href
                 */
                public void setHref(String href) {
                    this.href = href;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.href);
                }

                public Html() {
                }

                protected Html(Parcel in) {
                    this.href = in.readString();
                }

                public final Creator<Html> CREATOR = new Creator<Html>() {
                    public Html createFromParcel(Parcel source) {
                        return new Html(source);
                    }

                    public Html[] newArray(int size) {
                        return new Html[size];
                    }
                };
            }

            public class Issue implements Parcelable {

                @SerializedName("href")
                @Expose
                private String href;

                /**
                 *
                 * @return
                 * The href
                 */
                public String getHref() {
                    return href;
                }

                /**
                 *
                 * @param href
                 * The href
                 */
                public void setHref(String href) {
                    this.href = href;
                }


                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.href);
                }

                public Issue() {
                }

                protected Issue(Parcel in) {
                    this.href = in.readString();
                }

                public final Creator<Issue> CREATOR = new Creator<Issue>() {
                    public Issue createFromParcel(Parcel source) {
                        return new Issue(source);
                    }

                    public Issue[] newArray(int size) {
                        return new Issue[size];
                    }
                };
            }

            public class Comments implements Parcelable {

                @SerializedName("href")
                @Expose
                private String href;

                /**
                 *
                 * @return
                 * The href
                 */
                public String getHref() {
                    return href;
                }

                /**
                 *
                 * @param href
                 * The href
                 */
                public void setHref(String href) {
                    this.href = href;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.href);
                }

                public Comments() {
                }

                protected Comments(Parcel in) {
                    this.href = in.readString();
                }

                public final Creator<Comments> CREATOR = new Creator<Comments>() {
                    public Comments createFromParcel(Parcel source) {
                        return new Comments(source);
                    }

                    public Comments[] newArray(int size) {
                        return new Comments[size];
                    }
                };
            }

            public class ReviewComment implements Parcelable {

                @SerializedName("href")
                @Expose
                private String href;

                /**
                 *
                 * @return
                 * The href
                 */
                public String getHref() {
                    return href;
                }

                /**
                 *
                 * @param href
                 * The href
                 */
                public void setHref(String href) {
                    this.href = href;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.href);
                }

                public ReviewComment() {
                }

                protected ReviewComment(Parcel in) {
                    this.href = in.readString();
                }

                public final Creator<ReviewComment> CREATOR = new Creator<ReviewComment>() {
                    public ReviewComment createFromParcel(Parcel source) {
                        return new ReviewComment(source);
                    }

                    public ReviewComment[] newArray(int size) {
                        return new ReviewComment[size];
                    }
                };
            }

            public class ReviewComments implements Parcelable {

                @SerializedName("href")
                @Expose
                private String href;

                /**
                 *
                 * @return
                 * The href
                 */
                public String getHref() {
                    return href;
                }

                /**
                 *
                 * @param href
                 * The href
                 */
                public void setHref(String href) {
                    this.href = href;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.href);
                }

                public ReviewComments() {
                }

                protected ReviewComments(Parcel in) {
                    this.href = in.readString();
                }

                public final Creator<ReviewComments> CREATOR = new Creator<ReviewComments>() {
                    public ReviewComments createFromParcel(Parcel source) {
                        return new ReviewComments(source);
                    }

                    public ReviewComments[] newArray(int size) {
                        return new ReviewComments[size];
                    }
                };
            }

            public class Statuses implements Parcelable {

                @SerializedName("href")
                @Expose
                private String href;

                /**
                 *
                 * @return
                 * The href
                 */
                public String getHref() {
                    return href;
                }

                /**
                 *
                 * @param href
                 * The href
                 */
                public void setHref(String href) {
                    this.href = href;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.href);
                }

                public Statuses() {
                }

                protected Statuses(Parcel in) {
                    this.href = in.readString();
                }

                public final Creator<Statuses> CREATOR = new Creator<Statuses>() {
                    public Statuses createFromParcel(Parcel source) {
                        return new Statuses(source);
                    }

                    public Statuses[] newArray(int size) {
                        return new Statuses[size];
                    }
                };
            }

            public class Commits implements Parcelable {

                @SerializedName("href")
                @Expose
                private String href;

                /**
                 *
                 * @return
                 * The href
                 */
                public String getHref() {
                    return href;
                }

                /**
                 *
                 * @param href
                 * The href
                 */
                public void setHref(String href) {
                    this.href = href;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.href);
                }

                public Commits() {
                }

                protected Commits(Parcel in) {
                    this.href = in.readString();
                }

                public final Creator<Commits> CREATOR = new Creator<Commits>() {
                    public Commits createFromParcel(Parcel source) {
                        return new Commits(source);
                    }

                    public Commits[] newArray(int size) {
                        return new Commits[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeParcelable(this.self, flags);
                dest.writeParcelable(this.html, flags);
                dest.writeParcelable(this.issue, flags);
                dest.writeParcelable(this.comments, flags);
                dest.writeParcelable(this.reviewComments, flags);
                dest.writeParcelable(this.reviewComment, flags);
                dest.writeParcelable(this.commits, flags);
                dest.writeParcelable(this.statuses, flags);
            }

            public Links() {
            }

            protected Links(Parcel in) {
                this.self = in.readParcelable(Self.class.getClassLoader());
                this.html = in.readParcelable(Html.class.getClassLoader());
                this.issue = in.readParcelable(Issue.class.getClassLoader());
                this.comments = in.readParcelable(Comments.class.getClassLoader());
                this.reviewComments = in.readParcelable(ReviewComments.class.getClassLoader());
                this.reviewComment = in.readParcelable(ReviewComment.class.getClassLoader());
                this.commits = in.readParcelable(Commits.class.getClassLoader());
                this.statuses = in.readParcelable(Statuses.class.getClassLoader());
            }

            public final Creator<Links> CREATOR = new Creator<Links>() {
                public Links createFromParcel(Parcel source) {
                    return new Links(source);
                }

                public Links[] newArray(int size) {
                    return new Links[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(this.id);
            dest.writeString(this.url);
            dest.writeString(this.htmlUrl);
            dest.writeString(this.diffUrl);
            dest.writeString(this.patchUrl);
            dest.writeString(this.issueUrl);
            dest.writeString(this.commitsUrl);
            dest.writeString(this.reviewCommentsUrl);
            dest.writeString(this.reviewCommentUrl);
            dest.writeString(this.commentsUrl);
            dest.writeString(this.statusesUrl);
            dest.writeValue(this.number);
            dest.writeString(this.state);
            dest.writeString(this.title);
            dest.writeString(this.body);
            dest.writeParcelable(this.assignee, 0);
            dest.writeParcelable(this.milestone, flags);
            dest.writeValue(this.locked);
            dest.writeString(this.createdAt);
            dest.writeString(this.updatedAt);
            dest.writeString(this.closedAt);
            dest.writeString(this.mergedAt);
            dest.writeParcelable(this.head, flags);
            dest.writeParcelable(this.base, flags);
            dest.writeParcelable(this.Links, flags);
            dest.writeParcelable(this.user, flags);
        }

        public PullRequest() {
        }

        protected PullRequest(Parcel in) {
            this.id = (Integer) in.readValue(Integer.class.getClassLoader());
            this.url = in.readString();
            this.htmlUrl = in.readString();
            this.diffUrl = in.readString();
            this.patchUrl = in.readString();
            this.issueUrl = in.readString();
            this.commitsUrl = in.readString();
            this.reviewCommentsUrl = in.readString();
            this.reviewCommentUrl = in.readString();
            this.commentsUrl = in.readString();
            this.statusesUrl = in.readString();
            this.number = (Integer) in.readValue(Integer.class.getClassLoader());
            this.state = in.readString();
            this.title = in.readString();
            this.body = in.readString();
            this.assignee = in.readParcelable(Assignee.class.getClassLoader());
            this.milestone = in.readParcelable(Milestone.class.getClassLoader());
            this.locked = (Boolean) in.readValue(Boolean.class.getClassLoader());
            this.createdAt = in.readString();
            this.updatedAt = in.readString();
            this.closedAt = in.readString();
            this.mergedAt = in.readString();
            this.head = in.readParcelable(Head.class.getClassLoader());
            this.base = in.readParcelable(Base.class.getClassLoader());
            this.Links = in.readParcelable(PullRequest.Links.class.getClassLoader());
            this.user = in.readParcelable(User__.class.getClassLoader());
        }

        public final Creator<PullRequest> CREATOR = new Creator<PullRequest>() {
            public PullRequest createFromParcel(Parcel source) {
                return new PullRequest(source);
            }

            public PullRequest[] newArray(int size) {
                return new PullRequest[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.pullRequests);
    }

    public PullRequests() {
    }

    protected PullRequests(Parcel in) {
        this.pullRequests = new ArrayList<PullRequest>();
        in.readList(this.pullRequests, List.class.getClassLoader());
    }

    public static final Parcelable.Creator<PullRequests> CREATOR = new Parcelable.Creator<PullRequests>() {
        public PullRequests createFromParcel(Parcel source) {
            return new PullRequests(source);
        }

        public PullRequests[] newArray(int size) {
            return new PullRequests[size];
        }
    };
}














