package alexsander.com.br.desafioandroid.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import alexsander.com.br.desafioandroid.R;
import alexsander.com.br.desafioandroid.adapter.PullRequestsAdapter;
import alexsander.com.br.desafioandroid.domain.PullRequests;
import alexsander.com.br.desafioandroid.service.PullRequestService;

public class PullRequestActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private TextView tvOpened;
    private TextView tvClosed;
    private LinearLayoutManager mLayoutManager;
    private String creatorName;
    private String repositoryName;
    private List<PullRequests.PullRequest> mPullRequests;
    private int countOpened = 0;
    private int countClosed = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);

        if (getIntent() != null && getIntent().getStringExtra("repositoryName") != null
                && getIntent().getStringExtra("creatorName") != null) {
            repositoryName = getIntent().getStringExtra("repositoryName");
            creatorName = getIntent().getStringExtra("creatorName");
            mToolbar.setTitle(repositoryName);
        } else {
            finish();
        }

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_pull_request_list);
        tvOpened = (TextView) findViewById(R.id.tv_state_open);
        tvClosed = (TextView) findViewById(R.id.tv_state_close);
        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setHasFixedSize(true);

        if (savedInstanceState != null && savedInstanceState.containsKey("pullRequestsList") &&
                savedInstanceState.containsKey("countOpened") && savedInstanceState.containsKey("countClosed")) {
            mPullRequests = savedInstanceState.getParcelableArrayList("pullRequestsList");
            countOpened = savedInstanceState.getInt("countOpened");
            countClosed = savedInstanceState.getInt("countClosed");
            mRecyclerView.setAdapter(new PullRequestsAdapter(PullRequestActivity.this, mPullRequests, onClickPullRequest()));
            tvOpened.setText(countOpened + " opened");
            tvClosed.setText(" / " + countClosed + " closed");
        } else {
            taskPullRequests(creatorName, repositoryName);
        }
    }

    private void taskPullRequests (String creator, String repository) {
        TaskPullRequests taskPullRequests = new TaskPullRequests();
        taskPullRequests.execute(creator, repository);
    }

    private PullRequestsAdapter.PullRequestOnClickListener onClickPullRequest() {
        return new PullRequestsAdapter.PullRequestOnClickListener() {
            @Override
            public void onClickPullRequest(View view, int idx) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mPullRequests.get(idx).getHtmlUrl()));
                startActivity(intent);
            }
        };
    }

    private class TaskPullRequests extends AsyncTask<String, Void, List<PullRequests.PullRequest>> {

        @Override
        protected List<PullRequests.PullRequest> doInBackground(String... params) {
            mPullRequests = PullRequestService.getPullRequestList(params[0], params[1]);
            return mPullRequests;
        }

        @Override
        protected void onPostExecute(List<PullRequests.PullRequest> pullRequests) {
            super.onPostExecute(pullRequests);

            for (PullRequests.PullRequest pr : pullRequests) {
                if (pr.getState().equals("open")) {
                    countOpened++;
                } else if (pr.getState().equals("close")) {
                    countClosed++;
                }
            }
            tvOpened.setText(countOpened + " opened");
            tvClosed.setText(" / " + countClosed + " closed");
            mRecyclerView.setAdapter(new PullRequestsAdapter(PullRequestActivity.this, pullRequests, onClickPullRequest()));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("pullRequestsList", (ArrayList) mPullRequests);
        outState.putInt("countOpened", countOpened);
        outState.putInt("countClosed", countClosed);
    }
}
