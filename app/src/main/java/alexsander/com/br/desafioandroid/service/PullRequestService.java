package alexsander.com.br.desafioandroid.service;


import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;
import java.util.List;

import alexsander.com.br.desafioandroid.domain.PullRequests;

/**
 * Created by alexsander on 02/01/16.
 */
public class PullRequestService {
    private static final String BASE_URL = "https://api.github.com/repos/{creator}/{repository}/pulls";

    public static List<PullRequests.PullRequest> getPullRequestList(String creator, String repository) {
        HttpRequest request = HttpRequest
                .get(BASE_URL.replace("{creator}", creator).replace("{repository}", repository))
                .contentType("application/json");
        return parseJSON(request.body());
    }

    private static List<PullRequests.PullRequest> parseJSON(String request) {

        Gson gson = new Gson();
        Type listType = new TypeToken<List<PullRequests.PullRequest>>(){}.getType();
        PullRequests pullRequests = new PullRequests();
        pullRequests.setPullRequests((List<PullRequests.PullRequest>)gson.fromJson(request, listType));

        return pullRequests.getPullRequests();
    }
}
