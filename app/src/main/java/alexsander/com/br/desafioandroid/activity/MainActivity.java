package alexsander.com.br.desafioandroid.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import alexsander.com.br.desafioandroid.R;
import alexsander.com.br.desafioandroid.adapter.RepositoriesAdapter;
import alexsander.com.br.desafioandroid.domain.Repository;
import alexsander.com.br.desafioandroid.extras.EndlessRecyclerViewScrollListener;
import alexsander.com.br.desafioandroid.service.RepositoryService;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private List<Repository.Item> allRepositories = new ArrayList<>();
    private List<Repository.Item> moreRepositories;
    private LinearLayoutManager mLayoutManager;
    private int pageItem = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.activity_main_title);
        setSupportActionBar(mToolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_repository_list);
        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setHasFixedSize(true);

        if (savedInstanceState != null && savedInstanceState.containsKey("repositoryList")) {
            allRepositories = savedInstanceState.getParcelableArrayList("repositoryList");
            mRecyclerView.setAdapter(new RepositoriesAdapter(MainActivity.this, allRepositories, onClickItem()));
        } else {
            taskitem(pageItem);
        }

        mRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                taskitem(page);
            }
        });

    }

    // Search item
    private void taskitem(int page) {
        TaskItem taskitem = new TaskItem();
        taskitem.execute(page);
    }

    private RepositoriesAdapter.RepositoryOnClickListener onClickItem() {
        return new RepositoriesAdapter.RepositoryOnClickListener() {
            @Override
            public void onClickRepository(View view, int idx) {
                String repositoryName = allRepositories.get(idx).getName();
                String creatorName = allRepositories.get(idx).getOwner().getLogin();
                Intent intent = new Intent(MainActivity.this, PullRequestActivity.class);
                intent.putExtra("repositoryName", repositoryName);
                intent.putExtra("creatorName", creatorName);
                startActivity(intent);
            }
        };
    }

    private class TaskItem extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            Log.d("page", params[0] + "");
            moreRepositories = RepositoryService.getRepositoryList("language:Java", "stars", params[0] + "");
            for (int i = 0; i < moreRepositories.size(); i++) {
                allRepositories.add(moreRepositories.get(i));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mRecyclerView.getAdapter() == null) {
                mRecyclerView.setAdapter(new RepositoriesAdapter(MainActivity.this, allRepositories, onClickItem()));
            } else {
                RepositoriesAdapter adapter = (RepositoriesAdapter) mRecyclerView.getAdapter();
                int curSize = adapter.getItemCount();
                adapter.notifyItemRangeInserted(curSize, allRepositories.size() - 1);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("repositoryList", (ArrayList) allRepositories);
    }
}
