package alexsander.com.br.desafioandroid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import alexsander.com.br.desafioandroid.R;
import alexsander.com.br.desafioandroid.domain.Repository;

/**
 * Created by alexsander on 29/12/15.
 */
public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.RepositoriesViewHolder> {
    private Context context;
    private List<Repository.Item> items;
    private RepositoryOnClickListener repositoryOnClickListener;

    public RepositoriesAdapter(Context context, List<Repository.Item> items, RepositoryOnClickListener repositoryOnClickListener) {
        this.context = context;
        this.items = items;
        this.repositoryOnClickListener = repositoryOnClickListener;
    }

    @Override
    public RepositoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_repository_list, parent, false);
        RepositoriesViewHolder holder = new RepositoriesViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RepositoriesViewHolder holder, final int position) {
        Repository.Item i = items.get(position);
        holder.tvName.setText(i.getName());
        holder.tvDescription.setText(i.getDescription());
        holder.tvForks.setText(String.valueOf(i.getForksCount()));
        holder.tvStars.setText(String.valueOf(i.getStargazersCount()));
        holder.tvUsername.setText(i.getOwner().getLogin());
        holder.tvFullName.setText(i.getFullName());

        Picasso.with(context).load(i.getOwner().getAvatarUrl()).into(holder.ivUser);

        if (repositoryOnClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    repositoryOnClickListener.onClickRepository(holder.itemView, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.items != null ? this.items.size() : 0;
    }

    public interface RepositoryOnClickListener {
        void onClickRepository(View view, int idx);
    }

    public class RepositoriesViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvDescription;
        TextView tvForks;
        TextView tvStars;
        ImageView ivUser;
        TextView tvUsername;
        TextView tvFullName;

        public RepositoriesViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
            tvForks = (TextView) itemView.findViewById(R.id.tv_forks);
            tvStars = (TextView) itemView.findViewById(R.id.tv_stars);
            ivUser = (ImageView) itemView.findViewById(R.id.iv_user);
            tvUsername = (TextView) itemView.findViewById(R.id.tv_username);
            tvFullName = (TextView) itemView.findViewById(R.id.tv_full_name);
        }
    }
}
